/**
 * Example 5
 * =========
 * This example demonstrates how construct a 2d-3d parametrized surface. We use the sphere and a
 * torus as examples. A third example is a graph-representation of a surface.
 **/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <dune/common/parallel/mpihelper.hh>
#include <dune/curvedgrid/curvedgrid.hh>
#include <dune/curvedgrid/gridfunctions/analyticdiscretefunction.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/gmshreader.hh>
#include <dune/vtk/writers/unstructuredgridwriter.hh>
#include <dune/vtk/datacollectors/lagrangedatacollector.hh>

using namespace Dune;

template <int order, class Grid>
void write_grid (const Grid& grid, std::string filename)
{
  Vtk::LagrangeDataCollector dataCollector{grid.leafGridView(), order};
  Vtk::UnstructuredGridWriter vtkWriter{dataCollector, Vtk::FormatTypes::ASCII};
  vtkWriter.write(filename);
}

void sphere()
{
  // 1. Construct a reference grid
  YaspGrid<2, EquidistantOffsetCoordinates<double,2>> refGrid({0.0, 0.01}, {2.0*M_PI, M_PI-0.01}, {8, 8});

  // 2. Define the geometry mapping
  auto sphere = [](const FieldVector<double,2>& u)
  {
    return FieldVector<double,3>{
      std::cos(u[0])*std::sin(u[1]),
      std::sin(u[0])*std::sin(u[1]),
      std::cos(u[1])
    };
  };
  auto sphereGF = analyticDiscreteFunction(sphere, refGrid, /*order*/ 3);

  // 3. Wrap the reference grid to build a curved grid
  CurvedGrid grid{refGrid, sphereGF};

  write_grid<3>(grid, "sphere.vtu");
}

void torus()
{
  // 1. Construct a reference grid
  YaspGrid<2> refGrid({2.0*M_PI, 2.0*M_PI}, {8, 8});

  // 2. Define the geometry mapping
  auto torus = [](const FieldVector<double,2>& u)
  {
    return FieldVector<double,3>{
      (2.0 + std::cos(u[1]))*std::cos(u[0]),
      (2.0 + std::cos(u[1]))*std::sin(u[0]),
      std::sin(u[1])
    };
  };
  auto torusGF = analyticDiscreteFunction(torus, refGrid, /*order*/ 3);

  // 3. Wrap the reference grid to build a curved grid
  CurvedGrid grid{refGrid, torusGF};

  write_grid<3>(grid, "torus.vtu");
}

void graph()
{
  // 1. Construct a reference grid
  YaspGrid<2> refGrid({4.0*M_PI, 4.0*M_PI}, {8, 8});

  // 2. Define the geometry mapping
  auto g = [](const FieldVector<double,2>& u)
  {
    return FieldVector<double,3>{u[0], u[1],
      std::sin(u[0])*std::cos(u[1])
    };
  };
  auto gGF = analyticDiscreteFunction(g, refGrid, /*order*/ 3);

  // 3. Wrap the reference grid to build a curved grid
  CurvedGrid grid{refGrid, gGF};

  write_grid<3>(grid, "graph.vtu");
}

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  /*
   * This example shows parametrized surfaces, a sphere and a torus,
   * given by a 2d parameter domain and a mapping into 3d.
   * Note, to build a closed surface, the parameter domain must be
   * equipped with some kind of periodicity constraint, not shown
   * in this example. Use periodic grid wrappers or periodic basis
   * wrappers when attaching a discrete function space to the grid.
   */

  sphere();
  torus();
  graph();
}
