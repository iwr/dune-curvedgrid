/**
 * Example 7
 * =========
 * In the example we demonstrate that even codim 2 surfaces can be constructed. Therefore,
 * we parametrize a space curved, visiani's curved, by using a OneDGrid.
 **/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <bitset>
#include <cmath>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/curvedgrid/curvedgrid.hh>
#include <dune/grid/onedgrid.hh>
#include <dune/grid/io/file/vtk.hh>

using namespace Dune;

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  // 1. Construct a reference grid [-pi/2,pi/2]
  OneDGrid refGrid{100, -M_PI/2.0, M_PI/2.0};

  // 2. Define the geometry mapping
  auto viviani_curve = [](FieldVector<double,1> const& u) {
    return FieldVector<double,3>{
      std::cos(u) * std::cos(u),
      std::cos(u) * std::sin(u),
      std::sin(u)
    };
  };

  // 3. Wrap the reference grid to build a curved grid, with Lagrange elements of order 3
  CurvedGrid grid{refGrid, viviani_curve, 3};

  // 4. Write grid to vtu file
  SubsamplingVTKWriter writer{grid.leafGridView(), refinementIntervals(3)};
  writer.write("viviani_curve");
}