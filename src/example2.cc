/**
 * Example 2
 * =========
 * In this example we read a piecewise flat grid from a mesh file and use a simple projection
 * in order to generate a curved surface grid. This is demonstrated with the sphere parametrization.
 **/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <type_traits>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/curvedgrid/curvedgrid.hh>
#include <dune/grid/io/file/gmshreader.hh>

using namespace Dune;

#if HAVE_ALBERTA
  #include <dune/grid/albertagrid.hh>
  using GridType = AlbertaGrid<2,3>;
#elif HAVE_DUNE_FOAMGRID
  #include <dune/foamgrid/foamgrid.hh>
  using GridType = FoamGrid<2,3>;
#elif HAVE_DUNE_ALUGRID
  #include <dune/alugrid/grid.hh>
  using GridType = Dune::ALUGrid<2,3,Dune::simplex,Dune::conforming>;
#endif

int main(int argc, char** argv)
{
  using namespace Dune;
  MPIHelper::instance(argc, argv);

  // Construct a reference grid
  auto refGrid = GmshReader<GridType>::read(DUNE_GRID_PATH "sphere.msh");

  // Define the geometry mapping
  auto sphere = [](const auto& x) { return x / x.two_norm(); };

  // Wrap the reference grid to build a curved grid
  CurvedGrid grid{*refGrid, sphere, 3};
}