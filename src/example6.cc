/**
 * Example 6
 * =========
 * This example demonstrates that the CurvedGrid wrapper can also wrap another CurvedGrid.
 * We first create a simple 2d unit square domain. Then we use CurvedGrid to just scale the
 * the domain by the factor 2*pi and finally we wrap the scaled grid again to build a torus
 * domain.
 **/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <bitset>
#include <cmath>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/curvedgrid/curvedgrid.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk.hh>

namespace Dune {
namespace Curved {

struct Torus
{
  double r1 = 2.0, r2 = 1.0;

  auto operator() (FieldVector<double,2> const& u) const
  {
    return FieldVector<double,3>{
      (r1 + r2*std::cos(u[0])) * std::cos(u[1]),
      (r1 + r2*std::cos(u[0])) * std::sin(u[1]),
            r2*std::sin(u[0])
    };
  }

  friend auto derivative (Torus t)
  {
    return [r1=t.r1,r2=t.r2](FieldVector<double,2> const& u)
    {
      return FieldMatrix<double,3,2>{
        {-r2*std::sin(u[0])*std::cos(u[1]),-(r1 + r2*std::cos(u[0]))*std::sin(u[1])},
        {-r2*std::sin(u[0])*std::sin(u[1]), (r1 + r2*std::cos(u[0]))*std::cos(u[1])},
        { r2*std::cos(u[0]),                0.0}
      };
    };
  }
};

}}

int main(int argc, char** argv)
{
  using namespace Dune;

  MPIHelper::instance(argc, argv);

  // 1. Construct a reference grid [0,1]x[0,1]
  Dune::YaspGrid<2> grid0{ {1.0, 1.0}, {8, 16} };

  // 2. Stretch the grid
  CurvedGrid grid1{grid0, [](auto const& x) { return (2.0*M_PI)*x; }, 1};

  // 2. Define the geometry mapping
  auto torus = Curved::Torus{};

  // 3. Wrap the reference grid to build a curved grid, with Lagrange elements of order 3
  CurvedGrid grid2{grid1, torus};

  // 4. Write grid to vtu file
  SubsamplingVTKWriter writer{grid2.leafGridView(), refinementIntervals(3)};
  writer.write("torus2");
}
