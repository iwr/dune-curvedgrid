#ifndef DUNE_CURVEDGRID_IDSET_HH
#define DUNE_CURVEDGRID_IDSET_HH

#include <type_traits>

#include <dune/grid/common/indexidset.hh>

namespace Dune {
namespace Curved {

// IdSet
// -----

template <class Grid, class HostIdSet>
class IdSet
    : public Dune::IdSet<Grid, IdSet<Grid, HostIdSet>, typename HostIdSet::IdType>
{
  using Self = IdSet;
  using Super = Dune::IdSet<Grid, Self, typename HostIdSet::IdType>;

  using Traits = typename std::remove_const_t<Grid>::Traits;

public:
  template <int cc>
  struct Codim {
    using Entity = typename Traits::template Codim<cc>::Entity;
  };

  /// \brief type of the IDs
  using IdType = typename HostIdSet::IdType;

public:
  /// \brief construct the IdSet by storing the hostIdSet.
  explicit IdSet (const HostIdSet* hostIdSet)
    : hostIdSet_(hostIdSet)
  {}

  /// \brief get id of an entity `e`
  template <class Entity>
  IdType id (const Entity& e) const
  {
    return hostIdSet().id(e.impl().hostEntity());
  }

  /// \brief get id of an entity `e` of codim `cc`
  template <int cc>
  IdType id (const typename Codim<cc>::Entity& e) const
  {
    return hostIdSet().template id<cc>(e.impl().hostEntity());
  }

  /// \brief get the id of subentity `i` of co-dimension `codim` of a grid element `e`.
  IdType subId (const typename Codim<0>::Entity& e, int i, unsigned int codim) const
  {
    return hostIdSet().subId(e.impl().hostEntity(), i, codim);
  }

private:
  const HostIdSet& hostIdSet () const
  {
    assert( !!hostIdSet_ );
    return *hostIdSet_;
  }

  const HostIdSet* hostIdSet_ = nullptr;
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_IDSET_HH
