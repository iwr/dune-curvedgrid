#ifndef DUNE_CURVEDGRID_GEOMETRY_HH
#define DUNE_CURVEDGRID_GEOMETRY_HH

#include <cassert>
#include <functional>
#include <iterator>
#include <limits>
#include <optional>
#include <vector>

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/std/type_traits.hh>
#include <dune/curvedgeometry/curvedgeometry.hh>
#include <dune/curvedgeometry/localfunctiongeometry.hh>
#include <dune/geometry/affinegeometry.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>
#include <dune/localfunctions/lagrange/lfecache.hh>

#include "localgeometrywrapper.hh"

namespace Dune {
namespace Curved {

/// \brief Curved geometry implementation based on local basis function parametrization
/**
 *  Parametrization of the geometry by a differentiable localFunction
 *
 *  \tparam  ctype   Field-type of the coordinate vectors
 *  \tapram  mydim   Local dimension of the geometry domain
 *  \tparam  cdim    Dimension of the mapped global coordinates
 *  \tparam  LF      localFunction parametrizing the geometry
 *  \tparam  LG      localGeometry for coordinate transform from subEntity to element,
 *                   see \ref Impl::DefaultLocalGeometry and \ref Impl::LocalGeometryInterface
 *  \tparam  useInterpolation   If set to true, use a Lagrange interpolation of the geometry
 */
template <class ct, int mydim, int cdim, class LF, class LG, bool useInterpolation>
class Geometry;


template <class ct, int mydim, int cdim, class LF, class LG>
class Geometry<ct,mydim,cdim,LF,LG,true>
    : public CurvedGeometry<ct, mydim, cdim, CurvedGeometryTraits<ct, LagrangeLFECache<ct,ct,mydim>>>
{
  using Super = CurvedGeometry<ct, mydim, cdim, CurvedGeometryTraits<ct, LagrangeLFECache<ct,ct,mydim>>>;

  /// \brief type of the localFunction representation the geometry parametrization
  using LocalFunction = LF;

  /// \brief type of coordinate transformation for subEntities to codim=0 entities
  using LocalGeometry = LG;

  /// type of reference element
  using ReferenceElements = Dune::ReferenceElements<ct, mydim>;
  using ReferenceElement = typename ReferenceElements::ReferenceElement;

public:
  /// \brief constructor from localFunction to parametrize the geometry
  /**
   *  \param[in]  refElement     reference element for the geometry
   *  \param[in]  localFunction  localFunction for the parametrization of the geometry
   *                             (stored by value)
   *  \param[in]  args...        argument to construct the local geometry
   *
   */
  template <class... Args>
  Geometry (const ReferenceElement& refElement, int order, const LocalFunction& localFunction,
            Args&&... args)
    : Super(refElement,
      [lf=localFunction, lg=LocalGeometry{std::forward<Args>(args)...}](const auto& local)
      {
        return lf(lg.global(local));
      }, order)
  {}

  /// \brief constructor, forwarding to the other constructor that take a reference-element
  /**
   *  \param[in]  gt             geometry type
   *  \param[in]  localFunction  localFunction for the parametrization of the geometry
   *                             (stored by value)
   *  \param[in]  args...        argument to construct the local geometry
   */
  template <class... Args>
  Geometry (Dune::GeometryType gt, int order, const LocalFunction& localFunction, Args&&... args)
    : Geometry(ReferenceElements::general(gt), order, localFunction, std::forward<Args>(args)...)
  {}
};


// Specialization for the case that no interpolation should be used: Use LocalFunction `LF` directly as parametrization
template <class ct, int mydim, int cdim, class LF, class LG>
class Geometry<ct,mydim,cdim,LF,LG, false>
    : public LocalFunctionGeometry<LF,LG>
{
  using Super = LocalFunctionGeometry<LF,LG>;

  /// \brief type of the localFunction representation the geometry parametrization
  using LocalFunction = LF;

  /// \brief type of coordinate transformation for subEntities to codim=0 entities
  using LocalGeometry = LG;

  /// type of reference element
  using ReferenceElements = Dune::ReferenceElements<ct, mydim>;
  using ReferenceElement = typename ReferenceElements::ReferenceElement;

public:
  /// \brief constructor from localFunction to parametrize the geometry
  /**
   *  \param[in]  refElement     reference element for the geometry
   *  \param[in]  localFunction  localFunction for the parametrization of the geometry
   *                             (stored by value)
   *  \param[in]  args...        argument to construct the local geometry
   *
   */
  template <class LF_, class... Args>
  Geometry (const ReferenceElement& refElement, int, LF_&& localFunction, Args&&... args)
    : Super(refElement, std::forward<LF_>(localFunction), LocalGeometry{std::forward<Args>(args)...})
  {}

  /// \brief constructor, forwarding to the other constructor that take a reference-element
  /**
   *  \param[in]  gt             geometry type
   *  \param[in]  localFunction  localFunction for the parametrization of the geometry
   *                             (stored by value)
   *  \param[in]  args...        argument to construct the local geometry
   */
  template <class LF_, class... Args>
  Geometry (Dune::GeometryType gt, int, LF_&& localFunction, Args&&... args)
    : Super(gt, std::forward<LF_>(localFunction), LocalGeometry{std::forward<Args>(args)...})
  {}
};


#ifndef DOXYGEN

// Specialization for vertex geometries
template <class ct, int cdim, class LF, class LG>
class VertexGeometry
    : public AffineGeometry<ct,0,cdim>
{
  using Super = AffineGeometry<ct,0,cdim>;

  using LocalFunction = LF;
  using LocalGeometry = LG;

  /// \brief tolerance to numerical algorithms
  static ct tolerance () { return ct(16) * std::numeric_limits<ct>::epsilon(); }

  struct Construct {};

public:
  /// type of reference element
  using ReferenceElements = Dune::ReferenceElements<ct, 0>;
  using ReferenceElement = typename ReferenceElements::ReferenceElement;

  /// type of local coordinates
  using LocalCoordinate = FieldVector<ct, 0>;

  /// type of global coordinates
  using GlobalCoordinate = FieldVector<ct, cdim>;

protected:
  VertexGeometry (Construct, const ReferenceElement& refElement, const LocalFunction& lf,
                  const LocalGeometry& lg)
    : Super(refElement, std::vector<GlobalCoordinate>{lf(lg.global(refElement.position(0,0)))})
  {}

public:
  template <class... Args>
  VertexGeometry (const ReferenceElement& refElement, int, const LocalFunction& lf, Args&&... args)
    : VertexGeometry(Construct{}, refElement, lf, LocalGeometry(std::forward<Args>(args)...))
  {}

  template <class... Args>
  VertexGeometry (Dune::GeometryType gt, int, const LocalFunction& lf, Args&&... args)
    : VertexGeometry(Construct{}, ReferenceElements::general(gt), lf,
                LocalGeometry(std::forward<Args>(args)...))
  {}

  std::optional<LocalCoordinate> checkedLocal (const GlobalCoordinate& globalCoord) const
  {
    auto localCoord = Super::local(globalCoord);
    if ((globalCoord - Super::global(localCoord)).two_norm2() > tolerance())
      return {};

    return localCoord;
  }

  GlobalCoordinate normal (const LocalCoordinate& local) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "ERROR: normal() method only defined for edges in 2D and faces in 3D");
    return GlobalCoordinate(0);
  }

private:
  const VertexGeometry& flatGeometry () const { return *this; }
};


// Specialization for vertex geometries
template <class ct, int cdim, class LF, class LG>
class Geometry<ct,0,cdim,LF,LG,true>
    : public VertexGeometry<ct,cdim,LF,LG>
{
  using Super = VertexGeometry<ct,cdim,LF,LG>;
public:
  using Super::Super;
};

template <class ct, int cdim, class LF, class LG>
class Geometry<ct,0,cdim,LF,LG,false>
    : public VertexGeometry<ct,cdim,LF,LG>
{
  using Super = VertexGeometry<ct,cdim,LF,LG>;
public:
  using Super::Super;
};

#endif // DOXYGEN

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_GEOMETRY_HH
