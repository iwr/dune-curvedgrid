#ifndef DUNE_CURVEDGRID_INDEXSETS_HH
#define DUNE_CURVEDGRID_INDEXSETS_HH

#include <type_traits>
#include <vector>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/indexidset.hh>

namespace Dune {
namespace Curved {

// IndexSet
// --------

template <class Grid, class HostIndexSet>
class IndexSet
    : public Dune::IndexSet<Grid, IndexSet<Grid, HostIndexSet>, typename HostIndexSet::IndexType, typename HostIndexSet::Types>
{
  using Self = IndexSet;
  using Super = Dune::IndexSet<Grid, Self, typename HostIndexSet::IndexType, typename HostIndexSet::Types>;

  using Traits = typename std::remove_const_t<Grid>::Traits;
  using HostGrid = typename Traits::HostGrid;

public:
  /// \brief the dimension of the grid.
  static const int dimension = Traits::dimension;

  /// \brief type used for the indices.
  using IndexType = typename Super::IndexType;

  // should be std::vector<GeometryType>
  using Types = typename Super::Types;

public:
  IndexSet () noexcept = default;

  /// \brief construct the IndexSet by storing a pointer to the HostIndexSet.
  explicit IndexSet (const HostIndexSet* hostIndexSet) noexcept
    : hostIndexSet_(hostIndexSet)
  {}

  /// \brief shallow copy constructor
  explicit IndexSet (const IndexSet& other) noexcept
    : hostIndexSet_(other.hostIndexSet_)
  {}

  /// \brief shallow move constructor
  explicit IndexSet (IndexSet&& other) noexcept
    : hostIndexSet_(other.hostIndexSet_)
  {}

  IndexSet& operator= (const IndexSet& other) noexcept
  {
    hostIndexSet_ = other.hostIndexSet_;
    return *this;
  }

  IndexSet& operator= (IndexSet&& other) noexcept
  {
    hostIndexSet_ = other.hostIndexSet_;
    return *this;
  }

  /// \brief map entity to index.
  template <int cc>
  IndexType index (const typename Traits::template Codim<cc>::Entity& entity) const
  {
    return hostIndexSet().index(entity.impl().hostEntity());
  }

  using Super::index;

  /// \brief map the `i`th subentity of co-dimension `codim` of an entity `e` to an index.
  template <int cc>
  IndexType subIndex (const typename Traits::template Codim<cc>::Entity& e, int i, unsigned int codim) const
  {
    return hostIndexSet().subIndex(e.impl().hostEntity(), i, codim);
  }

  using Super::subIndex;

  /// \brief return total number of entities of given geometry type.
  IndexType size (GeometryType type) const
  {
    return hostIndexSet().size(type);
  }

  /// \brief return total number of entities of given `codim`.
  int size (int codim) const
  {
    return hostIndexSet().size(codim);
  }

  /// \brief return true if the given entity is contained in the entity-set
  template <class Entity>
  bool contains (const Entity& entity) const
  {
    return hostIndexSet().contains(entity.impl().hostEntity());
  }

  /// \brief return a vector of Geometry types of entities in the grid of given `codim`.
  Types types (int codim) const
  {
    return hostIndexSet().types(codim);
  }

private:
  const HostIndexSet& hostIndexSet () const
  {
    assert( !!hostIndexSet_ );
    return *hostIndexSet_;
  }

  const HostIndexSet* hostIndexSet_ = nullptr;
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_INDEXSETS_HH
