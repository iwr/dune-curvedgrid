#ifndef DUNE_CURVEDGRID_GRID_HH
#define DUNE_CURVEDGRID_GRID_HH

#include <dune/common/deprecated.hh>
#include <dune/common/referencehelper.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/curvedgrid/concepts.hh>
#include <dune/curvedgrid/gridfamily.hh>
#include <dune/curvedgrid/backuprestore.hh>
#include <dune/curvedgrid/datahandle.hh>
#include <dune/curvedgrid/gridfunctions/analyticgridfunction.hh>
#include <dune/curvedgrid/gridfunctions/gridfunction.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/geometrygrid/identity.hh>
#include <dune/grid/geometrygrid/persistentcontainer.hh>
#include <dune/grid/geometrygrid/grid.hh>

namespace Dune {
namespace Impl {

template <class HostGrid, class GF, bool useInterpolation>
struct CurvedGridBase
{
  using GridFunction = ResolveReference_t<GF>;
  using type = GridDefaultImplementation<
    HostGrid::dimension, Curved::DimRange<GridFunction>::value, typename HostGrid::ctype,
    Curved::GridFamily<HostGrid,GF,useInterpolation>
  >;
};

} // end namespace Impl

template <class HostGrid, class GF, bool useInterpolation>
using CurvedGridBase = typename Impl::CurvedGridBase<HostGrid,GF,useInterpolation>::type;


// CurvedGrid
// -----------------

/** \class CurvedGrid
 *  \brief grid wrapper replacing the geometries
 *  \ingroup CurvedGeo
 *
 *  CurvedGrid wraps another DUNE grid and replaces its geometry by an implementation
 *  of a CurvedGeometry.
 *
 *  \tparam HG     The host-grid type that is wrapped
 *  \tparam GF     GridViewFunction defined on a (flat) hostgrid
 *  \tparam useInterpolation  If true, use Lagrange interpolation of geometries [false]
 *
 *  \nosubgrouping
 */
template <class HG, class GF, bool useInterpolation = false>
class CurvedGrid;

/// \brief Generator for CurvedGrid from a grid-functions
template <class HG, class GF, bool useInterpolation = false,
  std::enable_if_t<Concept::isGridFunction<GF, HG>(), int> = 0>
auto curvedGrid (HG& hostGrid, GF&& gridFunction, int order = -1)
{
  assert(!useInterpolation || order > 0);

  using Grid = CurvedGrid<std::remove_const_t<HG>,std::decay_t<GF>,useInterpolation>;
  return Grid{hostGrid, std::forward<GF>(gridFunction), order};
}

/// \brief Generator for CurvedGrid from a callable
template <class HG, class F, bool useInterpolation = false,
  std::enable_if_t<not Concept::isGridFunction<F, HG>(), int> = 0>
auto curvedGrid (HG& hostGrid, F&& callable, int order = -1)
{
  using GlobalCoordinate = typename GridEntitySet<HG,0>::GlobalCoordinate;
  static_assert(Concept::isCallable<F, GlobalCoordinate>(), "Function must be callable");
  auto gridFct = analyticGridFunction<HG>(std::forward<F>(callable));

  using Grid = CurvedGrid<std::remove_const_t<HG>,decltype(gridFct),useInterpolation>;
  return Grid{hostGrid, std::move(gridFct), order};
}


template <class HG, class GF, bool useInterpolation>
class CurvedGrid
    : public CurvedGridBase<HG,GF,useInterpolation>
    , public Curved::BackupRestoreFacilities<CurvedGrid<HG,GF,useInterpolation>>
{
  using Self = CurvedGrid;
  using Super = CurvedGridBase<HG,GF,useInterpolation>;

  // friend declarations
  template <class, class> friend class Curved::IdSet;
  template <class, class> friend class Curved::IndexSet;

public:
  /// \brief type of the wrapped grid
  using HostGrid = std::remove_const_t<HG>;

  /// \brief raw type of the grid-function the curved grid is parametrized with.
  using GridFunction = ResolveReference_t<GF>;


  /** \name Traits
   *  \{ */

  /// \brief a collection of types related to the grid.
  using GridFamily = Curved::GridFamily<HG,GF,useInterpolation>;

  /// \brief type of the grid traits
  using Traits = typename GridFamily::Traits;

  /// \brief traits structure containing types for a codimension
  template <int codim>
  struct Codim;

  /** \} */


  /** \name Iterator Types
   *  \{ */

  /// \brief iterator over the grid hierarchy
  using HierarchicIterator = typename Traits::HierarchicIterator;

  /// \brief iterator over intersections with other entities on the leaf level
  using LeafIntersectionIterator = typename Traits::LeafIntersectionIterator;

  /// \brief iterator over intersections with other entities on the same level
  using LevelIntersectionIterator = typename Traits::LevelIntersectionIterator;

  /** \} */


  /** \name Grid View Types
   *  \{ */

  /// \brief type of view for leaf grid
  using LeafGridView = typename GridFamily::Traits::LeafGridView;

  /// \brief type of view for level grid
  using LevelGridView = typename GridFamily::Traits::LevelGridView;

  /** \} */


  /** \name Index and Id Set Types
   *  \{ */

  /// \brief type of leaf index set
  /**
   *  The LeafIndexSet is a wrapper around the corresponding LeafIndexSet of the
   *  HostGrid that forwards calls by passing the HostEntity.
   */
  using LeafIndexSet = typename Traits::LeafIndexSet;

  /// \brief type of level index set
  /**
   *  The LevelIndexSet is a wrapper around the corresponding LevelIndexSet of the
   *  HostGrid that forwards calls by passing the HostEntity.
   */
  using LevelIndexSet = typename Traits::LevelIndexSet;

  /// \brief type of global id set
  /**
   *  The GlobalIdSet is a wrapper around the corresponding GlobalIdSet of the
   *  HostGrid that forwards calls by passing the HostEntity.
   */
  using GlobalIdSet = typename Traits::GlobalIdSet;

  /// \brief type of local id set
  /**
   *  The LocalIdSet is a wrapper around the corresponding LocalIdSet of the
   *  HostGrid that forwards calls by passing the HostEntity.
   */
  using LocalIdSet = typename Traits::LocalIdSet;

  /** \} */

  /** \name Miscellaneous Types
   * \{ */

  /// \brief type of vector coordinates (e.g., double)
  using ctype = typename Traits::ctype;

  /// \brief communicator with all other processes having some part of the grid
  using Communication = typename Traits::Communication;

  /** \} */


  /** \name Construction and Destruction
   *  \{ */

  /// \brief constructor for host-grid given by reference
  /**
   *  The references to host grid is stored in the grid. It must be valid until this grid
   *  wrapper is destroyed.
   *
   *  \param[in]  hostGrid      reference to the grid to wrap
   *  \param[in]  gridFunction  mapping from global coordinates in the host geometry
   *                            to global coordinates in the curved geometry. Either passed as
   *                            value or in a `std::reference_wrapper` using `std::ref`.
   *                            It is stored by value.
   *  \param[in]  order         polynomial order of Lagrange functions used to interpolate
   *                            the geometry, if useInterpolation is true. (optional)
   */
  CurvedGrid (HostGrid& hostGrid, GF gridFunction, int order = -1)
    : hostGrid_(hostGrid)
    , gridFunction_(std::move(gridFunction))
    , levelIndexSets_(hostGrid_.maxLevel()+1)
    , order_(order)
  {
    assert(!useInterpolation != (order > 0));
  }

  /// \brief construct a CurvedGrid directly from a callable
  template <class F,
    std::enable_if_t<std::is_same_v<GF,AnalyticGridFunction<HostGrid,std::decay_t<F>>>, int> = 0>
  CurvedGrid (HostGrid& hostGrid, F&& callable, int order = -1)
    : CurvedGrid(hostGrid, analyticGridFunction<HostGrid>(std::forward<F>(callable)), order)
  {}

  /** \} */


  /** \name Size Methods
   *  \{ */

  /// \brief obtain maximal grid level
  /**
   *  Grid levels are numbered 0, ..., L, where L is the value returned by
   *  this method.
   *
   *  \returns maximal grid level
   */
  int maxLevel () const
  {
    return hostGrid().maxLevel();
  }

  /// \brief obtain number of entities on a level
  /**
   *  \param[in]  level  level to consider
   *  \param[in]  codim  codimension to consider
   *
   *  \returns number of entities of codimension `codim` on grid level `level`.
   */
  int size (int level, int codim) const
  {
    return levelGridView(level).size(codim);
  }

  /// \brief obtain number of leaf entities
  /**
   *  \param[in]  codim  codimension to consider
   *
   *  \returns number of leaf entities of codimension \em codim
   */
  int size (int codim) const
  {
    return leafGridView().size(codim);
  }

  /// \brief obtain number of entities on a level
  /**
   *  \param[in]  level  level to consider
   *  \param[in]  type   geometry type to consider
   *
   *  \returns number of entities with a geometry of type `type` on grid level `level`.
   */
  int size (int level, GeometryType type) const
  {
    return levelGridView(level).size(type);
  }

  /// \brief obtain number of leaf entities
  /**
   *  \returns number of leaf entities with a geometry of type `type`
   */
  int size (GeometryType type) const
  {
    return leafGridView().size(type);
  }

  /// \brief returns the number of boundary segments within the macro grid
  /**
   *  \returns number of boundary segments within the macro grid
   */
  std::size_t numBoundarySegments () const
  {
    return hostGrid().numBoundarySegments();
  }

  /** \} */


  /** \name Access to index and id sets
   *  \{ */

  /// \brief return const reference to the grids global id set.
  const GlobalIdSet& globalIdSet () const
  {
    if (!globalIdSet_)
      globalIdSet_ = std::make_unique<GlobalIdSet>(&hostGrid().globalIdSet());
    return *globalIdSet_;
  }

  /// \brief return const reference to the grids local id set.
  const LocalIdSet& localIdSet () const
  {
    if (!localIdSet_)
      localIdSet_ = std::make_unique<LocalIdSet>(&hostGrid().localIdSet());
    return *localIdSet_;
  }

  /// \brief return const reference to the grids level index set for level level.
  const LevelIndexSet& levelIndexSet (int level) const
  {
    assert(levelIndexSets_.size() == (size_t)(maxLevel()+1));

    if ((level < 0) || (level > maxLevel()))
      DUNE_THROW(GridError, "LevelIndexSet for nonexisting level " << level << " requested.");

    std::unique_ptr<LevelIndexSet>& levelIndexSet = levelIndexSets_[level];
    if (!levelIndexSet)
      levelIndexSet = std::make_unique<LevelIndexSet>(&hostGrid().levelIndexSet(level));
    return *levelIndexSet;
  }

  /// \brief return const reference to the grids leaf index set.
  const LeafIndexSet& leafIndexSet () const
  {
    if (!leafIndexSet_)
      leafIndexSet_ = std::make_unique<LeafIndexSet>(&hostGrid().leafIndexSet());
    return *leafIndexSet_;
  }

  /** \} */


  /** \name Adaptivity and grid refinement
   *  \{ */

  /// \brief refine the grid refCount times using the refinement rules of the hostGrid.
  void globalRefine (int refCount)
  {
    hostGrid().globalRefine(refCount);
    update();
  }

  /// \brief marks an entity in the hostGrid to be refined/coarsened in a subsequent adapt.
  bool mark (int refCount, const typename Codim<0>::Entity& entity)
  {
    return hostGrid().mark(refCount, getHostEntity<0>(entity));
  }

  /// \brief returns adaptation mark for given entity of the hostGrid.
  int getMark (const typename Codim<0>::Entity& entity) const
  {
    return hostGrid().getMark(getHostEntity<0>(entity));
  }

  /// \brief to be called after entities have been marked and before adapt() is called.
  bool preAdapt ()
  {
    return hostGrid().preAdapt();
  }

  /// \brief refine positive marked leaf entities, coarsen negative marked entities if possible.
  bool adapt ()
  {
    bool ret = hostGrid().adapt();
    update();
    return ret;
  }

  /// \brief called after grid adaption, to process left over information from adaption.
  void postAdapt ()
  {
    hostGrid().postAdapt();
  }

  /** \} */


  /** \name Parallel Data Distribution and Communication Methods
   *  \{ */

  /// \brief obtain Communication object
  /**
   *  The Communication object should be used to globally
   *  communicate information between all processes sharing this grid.
   *
   *  \note The Communication object returned is identical to the
   *        one returned by the host grid.
   */
  const Communication& comm () const
  {
    return hostGrid().comm();
  }

  /// \brief rebalance the load each process has to handle
  /**
   *  A parallel grid is redistributed such that each process has about
   *  the same load (e.g., the same number of leaf entities).
   *
   *  \returns \b true, if the grid has changed.
   */
  bool loadBalance ()
  {
    const bool gridChanged = hostGrid().loadBalance();
    if (gridChanged)
      update();
    return gridChanged;
  }

  /// \brief rebalance the load each process has to handle
  /**
   *  A parallel grid is redistributed such that each process has about
   *  the same load (e.g., the same number of leaf entities).
   *
   *  The data handle is used to communicate the data associated with
   *  entities that move from one process to another.
   *
   *  \param  datahandle  communication data handle (user defined)
   *
   *  \returns \b true, if the grid has changed.
   */

  template <class DataHandle, class Data>
  bool loadBalance (CommDataHandleIF<DataHandle, Data>& datahandle)
  {
    using DataHandleIF = CommDataHandleIF<DataHandle, Data>;
    using WrappedDataHandle = Curved::CommDataHandle<Self, DataHandleIF>;

    WrappedDataHandle wrappedDataHandle(*this, datahandle);
    const bool gridChanged = hostGrid().loadBalance(wrappedDataHandle);
    if (gridChanged)
      update();
    return gridChanged;
  }

  /** \} */


  /// \brief obtain Entity from EntitySeed
  template <class EntitySeed>
  typename Traits::template Codim<EntitySeed::codimension>::Entity entity (const EntitySeed& seed) const
  {
    using EntityImpl = typename Traits::template Codim<EntitySeed::codimension>::EntityImpl;
    return EntityImpl(*this, seed);
  }


  /** \name Grid Views
   *  \{ */

  /// \brief view for a grid level
  LevelGridView levelGridView (int level) const
  {
    using ViewImp = typename LevelGridView::GridViewImp;
    return LevelGridView(ViewImp(this, hostGrid().levelGridView(level)));
  }

  /// \brief view for the leaf grid
  LeafGridView leafGridView () const
  {
    using ViewImp = typename LeafGridView::GridViewImp;
    return LeafGridView(ViewImp(this, hostGrid().leafGridView()));
  }

  /** \} */


  /** \name Miscellaneous Methods
   *  \{ */

  /// \brief obtain constant reference to the host grid
  const HostGrid& hostGrid () const
  {
    return hostGrid_;
  }

  /// \brief obtain mutable reference to the host grid
  HostGrid& hostGrid ()
  {
    return hostGrid_;
  }

  /// \brief update grid caches
  /**
   *  This method has to be called whenever the underlying host grid changes.
   *
   *  \note If you adapt the host grid through this geometry grid's
   *        adaptation or load balancing methods, update is automatically
   *        called.
   */
  void update ()
  {
    levelIndexSets_.reserve(maxLevel()+1);
    for (int i = levelIndexSets_.size(); i < maxLevel()+1; ++i)
      levelIndexSets_.emplace_back(nullptr);
  }

  /// \brief obtain constant reference to the geometry mapping
  const GridFunction& gridFunction () const
  {
    return Dune::resolveRef(gridFunction_);
  }

  /// \brief obtain mutable reference to the geometry mapping
  GridFunction& gridFunction ()
  {
    return Dune::resolveRef(gridFunction_);
  }

  /// \brief return the interpolation order for the geometries
  int order () const
  {
    return order_;
  }

  /** \} */

protected:
  template <int codim>
  static const typename HostGrid::template Codim<codim>::Entity&
  getHostEntity(const typename Codim<codim>::Entity& entity)
  {
    return entity.impl().hostEntity();
  }

private:
  HostGrid& hostGrid_;
  GF gridFunction_;

  mutable std::vector<std::unique_ptr<LevelIndexSet>> levelIndexSets_;
  mutable std::unique_ptr<LeafIndexSet> leafIndexSet_;
  mutable std::unique_ptr<GlobalIdSet> globalIdSet_;
  mutable std::unique_ptr<LocalIdSet> localIdSet_;

  int order_; // polynomial order of local geometry interpolation
};

/// \brief Deduction guide for CurvedGrid from a grid-functions or callables if
/// the local-function of the grid-function should be used directly as geometry parametrization
template <class HG, class GF>
CurvedGrid (HG& hostGrid, GF&& gridFunction)
  -> CurvedGrid<std::remove_const_t<HG>, GridFunctionOf_t<HG,GF>, false>;

/// \brief Deduction guide for CurvedGrid from a grid-functions or callables if
/// interpolation should be used
template <class HG, class GF>
CurvedGrid (HG& hostGrid, GF&& gridFunction, int order)
  -> CurvedGrid<std::remove_const_t<HG>, GridFunctionOf_t<HG,GF>, true>;


// CurvedGrid::Codim
// ------------------------

template <class HostGrid, class GF, bool useInterpolation>
template <int codim>
struct CurvedGrid<HostGrid,GF,useInterpolation>::Codim
  : public Super::template Codim<codim>
{
  /** \name Entity types
   *  \{ */

  /** \brief type of entity
   *
   *  The entity is a model of Dune::Entity.
   */
  using Entity = typename Traits::template Codim< codim >::Entity;

  /** \} */

  /** \name Geometry Types
   *  \{ */

  /** \brief type of world geometry
   *
   *  Models the geomtry mapping of the entity, i.e., the mapping from the
   *  reference element into world coordinates.
   *
   *  The geometry is a model of Dune::Geometry, implemented through the
   *  generic geometries provided by dune-grid.
   */
  using Geometry = typename Traits::template Codim<codim>::Geometry;

  /** \brief type of local geometry
   *
   *  Models the geomtry mapping into the reference element of dimension
   *  \em dimension.
   *
   *  The local geometry is a model of Dune::Geometry, implemented through
   *  the generic geometries provided by dune-grid.
   */
  using LocalGeometry = typename Traits::template Codim<codim>::LocalGeometry;

  /** \} */

  /** \name Iterator Types
   *  \{ */

  template <PartitionIteratorType pitype>
  struct Partition
  {
    using LeafIterator
      = typename Traits::template Codim<codim>::template Partition<pitype>::LeafIterator;
    using LevelIterator
      = typename Traits::template Codim<codim>::template Partition<pitype>::LevelIterator;
  };

  /** \brief type of leaf iterator
   *
   *  This iterator enumerates the entites of codimension \em codim of a
   *  grid level.
   *
   *  The level iterator is a model of Dune::LevelIterator.
   */
  using LeafIterator = typename Partition<All_Partition>::LeafIterator;

  /** \brief type of level iterator
   *
   *  This iterator enumerates the entites of codimension \em codim of the
   *  leaf grid.
   *
   *  The leaf iterator is a model of Dune::LeafIterator.
   */
  using LevelIterator = typename Partition<All_Partition>::LevelIterator;

  /** \} */
};

} // end namespace Dune

#endif // DUNE_CURVEDGRID_GRID_HH
