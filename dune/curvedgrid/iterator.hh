#ifndef DUNE_CURVEDGRID_ITERATOR_HH
#define DUNE_CURVEDGRID_ITERATOR_HH

#include <cassert>
#include <type_traits>
#include <utility>

#include <dune/curvedgrid/entity.hh>
#include <dune/geometry/referenceelements.hh>

namespace Dune {
namespace Curved {

// Internal Forward Declarations
// -----------------------------

template <class HostGridView, int codim, PartitionIteratorType pitype, class Grid>
class Iterator;

template <class Grid >
class HierarchicIterator;


// Iterator
// --------

template <class HostGridView, int codim, PartitionIteratorType pitype, class GridType>
class Iterator
{
  using Traits = typename std::remove_const_t<GridType>::Traits;

public:
  static const int codimension = codim;

  using Grid = typename Traits::Grid;
  using GridFunction = typename Traits::GridFunction;
  using Entity = typename Traits::template Codim<codimension>::Entity;

private:
  using EntityImpl = Curved::Entity<codimension, Traits::dimension, GridType>;
  using HostEntityIterator = typename HostGridView::template Codim<codim>::template Partition<pitype>::Iterator;

public:
  Iterator () = default;

  Iterator (const GridFunction& gridFunction, HostEntityIterator hostEntityIterator, int order)
    : gridFunction_(&gridFunction)
    , hostEntityIterator_(std::move(hostEntityIterator))
    , order_(order)
  {}

  void increment ()
  {
    ++hostEntityIterator_;
  }

  bool equals (const Iterator& rhs) const
  {
    return hostEntityIterator_ == rhs.hostEntityIterator_;
  }

  Entity dereference () const
  {
    return EntityImpl(gridFunction(), *hostEntityIterator_, order_);
  }

  int level () const
  {
    return hostEntityIterator_.level();
  }

  const GridFunction& gridFunction () const
  {
    return *gridFunction_;
  }

  static Iterator begin (const GridFunction& gf, const HostGridView& hostGridView, int order)
  {
    HostEntityIterator hostEntityIterator = hostGridView.template begin<codimension, pitype>();
    return Iterator(gf, std::move(hostEntityIterator), order);
  }

  static Iterator end (const GridFunction& gf, const HostGridView& hostGridView, int order)
  {
    HostEntityIterator hostEntityIterator = hostGridView.template end<codimension, pitype>();
    return Iterator(gf, std::move(hostEntityIterator), order);
  }

private:
  const GridFunction* gridFunction_;
  HostEntityIterator hostEntityIterator_;
  int order_ = -1;
};


// HierarchicIterator
// ------------------

template <class GridType>
class HierarchicIterator
{
  using Traits = typename std::remove_const_t<GridType>::Traits;

public:
  static const int codimension = 0;

  using Grid = typename Traits::Grid;
  using GridFunction = typename Traits::GridFunction;
  using Entity = typename Traits::template Codim<codimension>::Entity;

private:
  using EntityImpl = Curved::Entity<codimension, Traits::dimension, GridType>;
  using HostEntityIterator = typename Grid::Traits::HostGrid::HierarchicIterator;

public:
  HierarchicIterator () = default;

  /// \brief Constructor. Stores a pointer to the grid and the iterator by value.
  HierarchicIterator (const GridFunction& gridFunction, HostEntityIterator hostEntityIterator, int order)
    : gridFunction_(&gridFunction)
    , hostEntityIterator_(std::move(hostEntityIterator))
    , order_(order)
  {}

  void increment ()
  {
    ++hostEntityIterator_;
  }

  bool equals (const HierarchicIterator& rhs) const
  {
    return hostEntityIterator_ == rhs.hostEntityIterator_;
  }

  Entity dereference () const
  {
    return EntityImpl(gridFunction(), *hostEntityIterator_, order_);
  }

  int level () const
  {
    return hostEntityIterator_.level();
  }

  const GridFunction& gridFunction () const
  {
    return *gridFunction_;
  }

private:
  const GridFunction* gridFunction_ = nullptr;
  HostEntityIterator hostEntityIterator_;
  int order_ = -1;
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_ITERATOR_HH
