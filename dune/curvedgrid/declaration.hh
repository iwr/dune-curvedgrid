#ifndef DUNE_CURVEDGRID_DECLARATION_HH
#define DUNE_CURVEDGRID_DECLARATION_HH

namespace Dune {

template <class HostGrid, class GridFunction, bool useInterpolation>
class CurvedGrid;

} // end namespace Dune

#endif // DUNE_CURVEDGRID_DECLARATION_HH
