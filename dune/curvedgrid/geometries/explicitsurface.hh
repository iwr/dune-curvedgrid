#ifndef DUNE_CURVEDGRID_EXPLICIT_SURFACE_PROJECTION_HH
#define DUNE_CURVEDGRID_EXPLICIT_SURFACE_PROJECTION_HH

#include <cmath>

#include <dune/curvedgrid/gridfunctions/analyticgridfunction.hh>
#include <dune/curvedgrid/utility/kdtree.hh>
#include <dune/curvedgrid/utility/vertexcache.hh>

#if COLLECTOR_BENCHMARK
  #include <dune/common/timer.hh>
#endif

namespace Dune {

/// \brief Closest-point projection to a surface given by a dune-grid
/**
 * Surface S is given by a triangles-only-grid in a 3d-world. The quality
 * of that grid is crucial to prevent artifacts in the projection. I.e. the
 * grid should be very fine-grained and triangles should not have acute
 * angles. A delaunay-triangulation would be perfect.
 **/
template <class T = double>
class ExplicitSurfaceProjection
{
  using Self = ExplicitSurfaceProjection<T>;
  using Domain = FieldVector<T, 3>;

  struct SurfaceElement
  {
    unsigned v0, v1, v2;
    SurfaceElement (unsigned v0, unsigned v1, unsigned v2)
      : v0(v0), v1(v1), v2(v2)
    {}
  };

  //calculates the distance between a given point and the line segment between 'lin0' and 'lin1'.
  static T distance_point_line_3d (const Domain& point, const Domain& lin0, const Domain& lin1,
                                                                            Domain &hit_vertex)
  {
    using std::sqrt;

    //the (complete) line through 'lin0' and 'lin1' is given by 'l = lin0 + t*g' with g as follows:
    T gx = lin1[0] - lin0[0];
    T gy = lin1[1] - lin0[1];
    T gz = lin1[2] - lin0[2];

    //the normal-vector from 'point' to l is 'v = lin0 + t*g - point' for some t which is determined
    // by 'v*g = 0'
    T t = (-gx * (lin0[0] - point[0]) - gy * (lin0[1] - point[1]) - gz * (lin0[2] - point[2]))
                                                                      / (gx * gx + gy * gy + gz * gz);

    //if the orthogonal projection of 'point' onto l is not on the given line segment then one of the
    //vertices 'lin0' or 'lin1' is the nearest point to 'point'
    if (t < 0) {
      //'lin0' is nearest
      hit_vertex[0] = lin0[0];
      hit_vertex[1] = lin0[1];
      hit_vertex[2] = lin0[2];
    } else if(t > 1) {
      //'lin1' is nearest
      hit_vertex[0] = lin1[0];
      hit_vertex[1] = lin1[1];
      hit_vertex[2] = lin1[2];
    } else {
      //orthogonal projection is nearest
      hit_vertex[0] = lin0[0] + t * gx;
      hit_vertex[1] = lin0[1] + t * gy;
      hit_vertex[2] = lin0[2] + t * gz;
    }

    //calculate distance
    T dx = point[0] - hit_vertex[0];
    T dy = point[1] - hit_vertex[1];
    T dz = point[2] - hit_vertex[2];

    return sqrt(dx * dx + dy * dy + dz * dz);
  }

  //calculates the distance between a given 'point' and the triangle given by 'tri0', 'tri1', 'tri2'.
  static T distance_point_triangle_3d (const Domain& point,
                    const Domain& tri0, const Domain& tri1, const Domain& tri2, Domain& hit_vertex)
  {
    using std::abs;
    using std::sqrt;
    const T epsilon = sqrt(std::numeric_limits<T>::epsilon());

    //triangle lies in plane 'p = tri0 + u*g + v*h', where g and h are defined as follows:
    T gx = tri1[0] - tri0[0];
    T gy = tri1[1] - tri0[1];
    T gz = tri1[2] - tri0[2];
    T hx = tri2[0] - tri0[0];
    T hy = tri2[1] - tri0[1];
    T hz = tri2[2] - tri0[2];

    //a vector from 'point' to p can be described by 'l = tri0 + u*g + v*h - point' and since we
    //search the distance we need such a vector that is orthogonal to p. Thus 'l*g = 0' and 'l*h = 0'.
    //We get a system of two equations of the form:
    //a1*u + b1*v = c1
    //b1*u + b2*v = c2
    //with a1, b1, b2, c1, c2 as follows:
    T b1 = gx * hx + gy * hy + gz * hz;
    T c2 = -hx * (tri0[0] - point[0]) - hy * (tri0[1] - point[1]) - hz * (tri0[2] - point[2]);
    T b2 = hx * hx + hy * hy + hz * hz;
    T c1 = -gx * (tri0[0] - point[0]) - gy * (tri0[1] - point[1]) - gz * (tri0[2] - point[2]);
    T a1 = gx * gx + gy * gy + gz * gz;

    //solve the system:
    T u, v;
    if (abs(b1) < epsilon) {
      v = c2 / b2;
      u = c1 / a1;
    } else {
      v = (c1 * b1 - c2 * a1) / (b1 * b1 - b2 * a1);
      u = (c1 - v * b1) / a1;
    }

    //now test whether the orthogonal projection of 'point' onto p lies inside the triangle
    T distance;
    if (u >= 0 && v >= 0 && u + v <= 1) {
      //yes, inside. The length of the normal-vector is the desired distance
      T fx = hit_vertex[0] = tri0[0] + u * gx + v * hx;
      T fy = hit_vertex[1] = tri0[1] + u * gy + v * hy;
      T fz = hit_vertex[2] = tri0[2] + u * gz + v * hz;
      fx -= point[0];
      fy -= point[1];
      fz -= point[2];
      distance = sqrt(fx * fx + fy * fy + fz * fz);
    } else {
      //no, not inside. The desired distance is the distance to one of the triangle-edges
      distance = distance_point_line_3d(point, tri0, tri1, hit_vertex);
      Domain current_hit_vertex;
      T current_distance = distance_point_line_3d(point, tri1, tri2, current_hit_vertex);
      if (current_distance < distance) {
        distance = current_distance;
        hit_vertex = current_hit_vertex;
      }
      current_distance = distance_point_line_3d(point, tri2, tri0, current_hit_vertex);
      if (current_distance < distance) {
        distance = current_distance;
        hit_vertex = current_hit_vertex;
      }
    }
    return distance;
  }

public:
  // constructor using a pre-loaded Dune-surface-grid
  template <class Grid>
  ExplicitSurfaceProjection (const Grid& grid, bool cached = true)
    : vertices_(grid.size(Grid::dimension))
    , adjacent_elements_(grid.size(Grid::dimension))
    , minCorner_(std::numeric_limits<T>::max())
    , maxCorner_(std::numeric_limits<T>::min())
    , cached_(cached)
  {
    const auto& indexSet = grid.leafGridView().indexSet();

    //collect vertices for kd-tree
    for (const auto& v : vertices(grid.leafGridView()))
    {
      const auto& vertex = v.geometry().corner(0);
      for (int i = 0; i < Grid::dimensionworld; ++i) {
        if (vertex[i] < minCorner_[i]) minCorner_[i] = vertex[i];
        if (vertex[i] > maxCorner_[i]) maxCorner_[i] = vertex[i];
      }
      vertices_[indexSet.index(v)] = vertex;
    }

    //collect elements and element-adjacency for closest-point-tests
    elements_.reserve(grid.size(0));
    unsigned el_index = 0;
    for (const auto& el : elements(grid.leafGridView()))
    {
      unsigned v0 = indexSet.subIndex(el, 0, Grid::dimension);
      unsigned v1 = indexSet.subIndex(el, 1, Grid::dimension);
      unsigned v2 = indexSet.subIndex(el, 2, Grid::dimension);
      elements_.emplace_back(v0, v1, v2);
      adjacent_elements_[v0].push_back(el_index);
      adjacent_elements_[v1].push_back(el_index);
      adjacent_elements_[v2].push_back(el_index);
      ++el_index;
    }

    kdtree_.emplace(vertices_, minCorner_, maxCorner_);
    kdtree_->update();

    if (cached_) {
      Domain c_expansion = (maxCorner_ - minCorner_) * 0.1;
      cache_.emplace(minCorner_ - c_expansion, maxCorner_ + c_expansion);
    }
  }

  ExplicitSurfaceProjection (const Self& other) = delete;
//     : vertices_(other.vertices_)
//     , adjacent_elements_(other.adjacent_elements_)
//     , elements_(other.elements_)
//     , minCorner_(other.minCorner_)
//     , maxCorner_(other.maxCorner_)
//     , cached_(other.cached_)
//     , cache_(other.cache_)
// #if COLLECTOR_BENCHMARK
//     , inputs_(other.inputs_)
// #endif
//   {
//     kdtree_.emplace(vertices_, minCorner_, maxCorner_);
//     kdtree_->update();
//   }

  ExplicitSurfaceProjection (Self&& other) = delete;
//     : vertices_(std::move(other.vertices_))
//     , adjacent_elements_(std::move(other.adjacent_elements_))
//     , elements_(std::move(other.elements_))
//     , minCorner_(other.minCorner_)
//     , maxCorner_(other.maxCorner_)
//     , cached_(other.cached_)
//     , cache_(std::move(other.cache_))
// #if COLLECTOR_BENCHMARK
//     , inputs_(std::move(other.inputs_))
// #endif
//   {
//     // update pointer
//     kdtree_.emplace(vertices_, minCorner_, maxCorner_);
//     kdtree_->update();
//   }

  Self& operator= (const Self&) = delete;
  Self& operator= (Self&&) = delete;

  void resetCache ()
  {
    if(cached_) cache_->clear();
  }

  Domain closestPoint (const Domain& x, std::size_t closest_vertex) const
  {
    T min_dist = std::numeric_limits<T>::max();
    Domain hit_vertex, best_hit_vertex;
    for(unsigned el_index : adjacent_elements_[closest_vertex]){
      const SurfaceElement &srf_el = elements_[el_index];
      T current_dist = distance_point_triangle_3d(x,
               vertices_[srf_el.v0], vertices_[srf_el.v1], vertices_[srf_el.v2], hit_vertex);
      if(current_dist < min_dist){
        min_dist = current_dist;
        best_hit_vertex = hit_vertex;
      }
    }
    return best_hit_vertex;
  }

  //evaluation of the closest-point projection
  Domain operator() (const Domain& x) const
  {
    #if COLLECTOR_BENCHMARK != 0
      inputs_.push_back(x);
    #endif

    Domain result;
    if (!cached_ || !cache_->find(x, result)){
      //find closest surface-vertex
      std::vector<std::size_t> outIndices(1);
      std::vector<T> outDistSq(1);
      kdtree_->query(x, 1, outIndices, outDistSq);

      //check surface-elements adjacent to closest surface-vertex for actual closest-point
      result = closestPoint(x, outIndices[0]);

      //cache result
      if (cached_) cache_->insert(x, result);
    }

    return result;
  }

#if COLLECTOR_BENCHMARK
  void collectorBenchmark (bool fresh_cache=true) const
  {
    if (cached_ && fresh_cache) cache_->clear();
    int uncached_cnt = 0;
    Timer t;
    for (auto &input : inputs_){
      Domain result;
      if (!cached_ || !cache_->find(input, result)) {
        //find closest surface-vertex
        std::vector<std::size_t> outIndices(1);
        std::vector<T> outDistSq(1);
        kdtree_->query(input, 1, outIndices, outDistSq);

        //check surface-elements adjacent to closest surface-vertex for actual closest-point
        result = closestPoint(input, outIndices[0]);

        //cache result
        if (cached_) cache_->insert(input, result);
        ++uncached_cnt;
      }
      input = result;
    }
    std::cout << "surfGF time: " << t.elapsed() << ", count: "<< inputs_.size()
                                 << ", uncached: " << uncached_cnt << std::endl;
  }
#endif

private:
  std::vector<Domain> vertices_;
  std::vector<std::vector<unsigned>> adjacent_elements_;
  std::vector<SurfaceElement> elements_;
  Domain minCorner_;
  Domain maxCorner_;
  std::optional<KDTree<T>> kdtree_;
  bool cached_;
  mutable std::optional<VertexCache<Domain, T>> cache_;
  #if COLLECTOR_BENCHMARK != 0
    mutable std::vector<Domain> inputs_;
  #endif
};

// deduction guide
template <class Grid>
ExplicitSurfaceProjection(Grid const&, bool = false)
  -> ExplicitSurfaceProjection<typename Grid::ctype>;

/// \brief construct a grid function representing an explicit surface parametrization
template <class Grid>
auto explicitSurfaceProjection (Grid& grid, bool cached = true)
{
  return ExplicitSurfaceProjection<typename Grid::ctype>{grid, cached};
}

} // end namespace Dune

#endif // DUNE_CURVEDGRID_EXPLICIT_SURFACE_PROJECTION_HH
