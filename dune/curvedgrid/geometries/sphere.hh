#ifndef DUNE_CURVEDGRID_SPHERE_GRIDFUNCTION_HH
#define DUNE_CURVEDGRID_SPHERE_GRIDFUNCTION_HH

#include <cmath>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/curvedgrid/gridfunctions/analyticgridfunction.hh>

namespace Dune {

/// \brief Functor representing a sphere in dim
template <int dim, class T = double>
class SphereProjection
{
  using Domain = FieldVector<T,dim>;
  using Jacobian = FieldMatrix<T,dim,dim>;

  T radius_;

public:
  SphereProjection (T radius)
    : radius_(radius)
  {}

  /// \brief project the coordinate to the sphere at origin with `radius`
  Domain operator() (const Domain& x) const
  {
    return x * (radius_ / x.two_norm());
  }

  /// \brief derivative of the projection
  friend auto derivative (const SphereProjection& sphere)
  {
    return [r=sphere.radius_](const Domain& x)
    {
      Jacobian out;
      auto nrm = x.two_norm();
      for (int i = 0; i < dim; ++i)
        for (int j = 0; j < dim; ++j)
          out[i][j] = r * ((i == j ? 1 : 0) - (x[i]/nrm) * (x[j]/nrm)) / nrm;
      return out;
    };
  }

  /// \brief normal vector = x/|x|
  Domain normal (const Domain& x) const
  {
    return x / x.two_norm();
  }

  /// \brief mean curvature of the sphere = 1/R
  T mean_curvature (const Domain& /*x*/) const
  {
    return T(1)/radius_;
  }

  /// \brief surface area of the sphere
  T area () const
  {
    return dim == 2 ? 2*M_PI*radius_ :
           dim == 3 ? 4*M_PI*radius_*radius_ : 0;
  }
};

/// \brief construct a grid function representing a sphere parametrization
template <class Grid, class T>
auto sphereGridFunction (T radius)
{
  return analyticGridFunction<Grid>(SphereProjection<Grid::dimensionworld,T>{radius});
}

} // end namespace Dune

#endif // DUNE_CURVEDGRID_SPHERE_GRIDFUNCTION_HH
