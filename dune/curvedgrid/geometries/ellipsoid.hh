#ifndef DUNE_CURVEDGRID_ELLIPSOID_GRIDFUNCTION_HH
#define DUNE_CURVEDGRID_ELLIPSOID_GRIDFUNCTION_HH

#include <cmath>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/math.hh>
#include <dune/curvedgrid/gridfunctions/analyticgridfunction.hh>

#include "implicitsurface.hh"

namespace Dune {

// Ellipsoid functor
template <class T = double>
class EllipsoidProjection
{
  using Domain = FieldVector<T,3>;
  using Jacobian = FieldMatrix<T,3,3>;

  // Implicit function representation of the ellipsoid surface
  struct Phi
  {
    T a_, b_, c_;

    // phi(x,y,z) = (x/a)^2 + (y/b)^2 + (z/c)^2 = 1
    T operator() (const FieldVector<T,3>& x) const
    {
      return x[0]*x[0]/(a_*a_) + x[1]*x[1]/(b_*b_) + x[2]*x[2]/(c_*c_) - 1;
    }

    // grad(phi)
    friend auto derivative (Phi phi)
    {
      return [a=phi.a_,b=phi.b_,c=phi.c_](const Domain& x) -> Domain
      {
        return { T(2*x[0]/(a*a)), T(2*x[1]/(b*b)), T(2*x[2]/(c*c)) };
      };
    }
  };

public:
  /// \brief Constructor of ellipsoid by major axes
  EllipsoidProjection (T a, T b, T c)
    : a_(a)
    , b_(b)
    , c_(c)
    , implicit_(Phi{a,b,c},100)
  {}

  /// \brief project the coordinate to the ellipsoid
  Domain operator() (const Domain& x) const
  {
    return implicit_(x);
  }

  /// \brief Normal vector
  Domain normal (const Domain& X) const
  {
    using std::sqrt;
    T x = X[0], y = X[1], z = X[2];
    T a2 = a_*a_, b2 = b_*b_, c2 = c_*c_;

    auto div = sqrt(b2*b2*c2*c2*x*x + a2*a2*c2*c2*y*y + a2*a2*b2*b2*z*z);
    return { b2*c2*x/div , a2*c2*y/div , a2*b2*z/div };
  }

  /// \brief Mean curvature
  T mean_curvature (const Domain& X) const
  {
    using std::sqrt; using std::abs;
    T x = X[0], y = X[1], z = X[2];
    T x2 = x*x, y2 = y*y, z2 = z*z;
    T a2 = a_*a_, b2 = b_*b_, c2 = c_*c_;

    auto div = 2*a2*b2*c2 * power(sqrt(x2/(a2*a2) + y2/(b2*b2) + z2/(c2*c2)), 3);
    return abs(x2 + y2 + z2 - a2 - b2 - c2)/div;
  }

  /// \brief Gaussian curvature
  T gauss_curvature (const Domain& X) const
  {
    T x = X[0], y = X[1], z = X[2];
    T x2 = x*x, y2 = y*y, z2 = z*z;
    T a2 = a_*a_, b2 = b_*b_, c2 = c_*c_;

    auto div = a2*b2*c2 * power(x2/(a2*a2) + y2/(b2*b2) + z2/(c2*c2), 2);
    return T(1)/div;
  }

private:
  T a_, b_, c_;
  SimpleImplicitSurfaceProjection<Phi> implicit_;
};

/// \brief construct a grid function representing a sphere parametrization
template <class Grid, class T>
auto ellipsoidGridFunction (T a, T b, T c)
{
  return analyticGridFunction<Grid>(EllipsoidProjection<T>{a,b,c});
}

} // end namespace Dune

#endif // DUNE_CURVEDGRID_ELLIPSOID_GRIDFUNCTION_HH
