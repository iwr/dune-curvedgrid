#ifndef DUNE_CURVEDGRID_TORUS_GRIDFUNCTION_HH
#define DUNE_CURVEDGRID_TORUS_GRIDFUNCTION_HH

#include <cassert>
#include <cmath>
#include <limits>
#include <type_traits>

#include <dune/common/math.hh>
#include <dune/curvedgrid/gridfunctions/analyticgridfunction.hh>

namespace Dune {

// torus functor
template <class T = double>
class TorusProjection
{
  using Domain = FieldVector<T,3>;
  using Jacobian = FieldMatrix<T,3,3>;

  // Implicit function representation of the torus surface
  struct Phi
  {
    T R, r;

    // phi(x,y,z) = (sqrt(x^2 + y^2) - R)+2 + z^2 = r^2
    T operator() (const Domain& x) const
    {
      using std::sqrt;
      T phi0 = sqrt(x[0]*x[0] + x[1]*x[1]);
      return (phi0 - R)*(phi0 - R) + x[2]*x[2] - r*r;
    }

    // grad(phi)
    friend auto derivative (const Phi& phi)
    {
      return [R=phi.R,r=phi.r](const Domain& x) -> Domain
      {
        using std::sqrt;
        T phi0 = sqrt(x[0]*x[0] + x[1]*x[1]);
        return {
          -2*R*x[0]/phi0 + 2*x[0] ,
          -2*R*x[1]/phi0 + 2*x[1] ,
            2*x[2]
        };
      };
    }
  };

public:
  /// \brief Construction of the torus function by major and minor radius
  TorusProjection (T R, T r)
    : R_(R)
    , r_(r)
  {}

  /// \brief Closest point projection
  Domain operator() (const Domain& x) const
  {
    using std::sqrt;
    auto scale1 = R_ / sqrt(x[0]*x[0] + x[1]*x[1]);
    Domain center{x[0] * scale1, x[1] * scale1, 0};
    Domain out{x[0] - center[0], x[1] - center[1], x[2]};
    out *= r_ / out.two_norm();

    return out + center;
  }


  /// \brief Normal vector
  Domain normal (const Domain& x) const
  {
    using std::sqrt;
    auto X = (*this)(x);
    T factor = (1 - R_/sqrt(X[0]*X[0] + X[1]*X[1]))/r_;
    return { X[0]*factor, X[1]*factor, X[2]/r_ };
  }

  /// \brief Mean curvature
  T mean_curvature (const FieldVector<T,3>& x) const
  {
    using std::sqrt;
    auto X = (*this)(x);
    return (2 - R_/sqrt(X[0]*X[0] + X[1]*X[1]))/(2*r_);
  }

  /// \brief surface area of the torus = 4*pi^2*R*r
  T area () const
  {
    return 4*M_PI*M_PI*R_*r_;
  }

private:
  T R_, r_;
};

/// \brief construct a grid function representing a torus parametrization
template <class Grid, class T>
auto torusGridFunction (T R, T r)
{
  return analyticGridFunction<Grid>(TorusProjection<T>{R,r});
}

} // end namespace Dune

#endif // DUNE_CURVEDGRID_TORUS_GRIDFUNCTION_HH
