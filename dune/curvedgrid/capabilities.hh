#ifndef DUNE_CURVEDGRID_CAPABILITIES_HH
#define DUNE_CURVEDGRID_CAPABILITIES_HH

#include <cassert>
#include <utility>

#include <dune/common/hybridutilities.hh>
#include <dune/curvedgrid/declaration.hh>
#include <dune/curvedgrid/gridfunctions/gridfunction.hh>
#include <dune/grid/common/capabilities.hh>
#include <dune/grid/geometrygrid/capabilities.hh>

// Capabilities
// ------------

namespace Dune {
namespace Capabilities {

// Capabilities from dune-grid
// ---------------------------

template <class HostGrid, class GridFunction, bool useInterpolation>
struct hasSingleGeometryType<CurvedGrid<HostGrid,GridFunction,useInterpolation>>
{
  static const bool v = hasSingleGeometryType<HostGrid>::v;
  static const unsigned int topologyId = hasSingleGeometryType<HostGrid>::topologyId;
};

template <class HostGrid, class GridFunction, bool useInterpolation, int codim>
struct hasEntity<CurvedGrid<HostGrid,GridFunction,useInterpolation>, codim>
{
  static const bool v = hasEntity<HostGrid,codim>::v;
};

template <class HostGrid, class GridFunction, bool useInterpolation, int codim>
struct hasEntityIterator<CurvedGrid<HostGrid,GridFunction,useInterpolation>, codim>
{
  static const bool v = hasEntityIterator<HostGrid,codim>::v;
};

/// \brief Implements geometry only for codim=0 entity
template <class HostGrid, class GridFunction, bool useInterpolation, int codim>
struct hasGeometry<CurvedGrid<HostGrid,GridFunction,useInterpolation>, codim>
{
  static const bool v = (codim == 0);
};

template <class HostGrid, class GridFunction, bool useInterpolation, int codim>
struct canCommunicate<CurvedGrid<HostGrid,GridFunction,useInterpolation>, codim>
{
  static const bool v = canCommunicate<HostGrid, codim>::v && hasEntity<HostGrid, codim>::v;
};

/// \brief Conformity of the grid is not guaranteed since it depends on the GridFunction that
/// \brief must be continuous in that case.
template <class HostGrid, class GridFunction, bool useInterpolation>
struct isLevelwiseConforming<CurvedGrid<HostGrid,GridFunction,useInterpolation>>
{
  static const bool v = false;
};

/// \brief Conformity of the grid is not guaranteed since it depends on the GridFunction that
/// \brief must be continuous in that case.
template <class HostGrid, class GridFunction, bool useInterpolation>
struct isLeafwiseConforming<CurvedGrid<HostGrid,GridFunction,useInterpolation>>
{
  static const bool v = false;
};

/// \brief Implements only partial backup-restore facilities, since the gridfunction is not
/// \brief backuped automatically.
template <class HostGrid, class GridFunction, bool useInterpolation>
struct hasBackupRestoreFacilities<CurvedGrid<HostGrid,GridFunction,useInterpolation>>
{
  static const bool v = hasBackupRestoreFacilities<HostGrid>::v;
};


// hasHostEntity
// -------------

template <class HostGrid, class GridFunction, bool useInterpolation, int codim>
struct hasHostEntity<CurvedGrid<HostGrid,GridFunction,useInterpolation>, codim>
{
  static const bool v = hasEntity<HostGrid, codim>::v;
};

} // end namespace Capabilities
} // end namespace Dune

#endif // DUNE_CURVEDGRID_CAPABILITIES_HH
