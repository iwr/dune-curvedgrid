#ifndef DUNE_CURVEDGRID_LOCALGEOMETRYWRAPPER_HH
#define DUNE_CURVEDGRID_LOCALGEOMETRYWRAPPER_HH

#include <functional>
#include <utility>

namespace Dune {
namespace Curved {

/// type-erased wrapper for local geometries
/**
 * \tparam LG  Type of a local geometry
 **/
template <class LG>
struct LocalGeometryWrapper
{
  using LocalCoordinate = typename LG::LocalCoordinate;
  using GlobalCoordinate = typename LG::GlobalCoordinate;
  using JacobianTransposed = typename LG::JacobianTransposed;

  template <class LocalGeometry>
  LocalGeometryWrapper(const LocalGeometry& lg)
    : global([lg](const auto& local) { return lg.global(local); })
    , jacobianTransposed([lg](const auto& local) { return lg.jacobianTransposed(local); })
  {}

  template <class GlobalFct, class JacobianTransposedFct>
  LocalGeometryWrapper(GlobalFct&& g, JacobianTransposedFct&& jt)
    : global(std::forward<GlobalFct>(g))
    , jacobianTransposed(std::forward<JacobianTransposedFct>(jt))
  {}

  std::function<GlobalCoordinate(LocalCoordinate)> global;
  std::function<JacobianTransposed(LocalCoordinate)> jacobianTransposed;
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_LOCALGEOMETRYWRAPPER_HH
