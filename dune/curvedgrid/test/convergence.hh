#pragma once
#include <iostream>

#include <dune/geometry/quadraturerules.hh>
#include <dune/localfunctions/lagrange/lagrangesimplex.hh>

namespace Dune {

// Test Hausdorff-distance
template <class Grid, class Projection>
typename Grid::ctype inf_error (const Grid& grid, const Projection& projection, int quad_order = 8)
{
  std::cout << "inf_error..." << std::endl;
  using QuadProvider = Dune::QuadratureRules<typename Grid::ctype, 2>;
  typename Grid::ctype dist = 0;
  for (const auto& element : elements(grid.leafGridView()))
  {
    auto geometry = element.geometry();

    const auto& quadRule = QuadProvider::rule(element.type(), quad_order);
    for (const auto& qp : quadRule) {
      auto x = geometry.global(qp.position());
      auto y = projection(x);
      dist = std::max(dist, (x - y).two_norm());
    }
  }

  return dist;
}

// Test integrated Hausdorff-distance
template <class Grid, class Projection>
typename Grid::ctype L2_error (const Grid& grid, const Projection& projection, int quad_order = 8)
{
  using std::sqrt;
  std::cout << "L2_error..." << std::endl;
  using QuadProvider = Dune::QuadratureRules<typename Grid::ctype, 2>;
  typename Grid::ctype dist = 0;
  for (const auto& element : elements(grid.leafGridView()))
  {
    auto geometry = element.geometry();

    const auto& quadRule = QuadProvider::rule(element.type(), quad_order);
    for (const auto& qp : quadRule) {
      auto x = geometry.global(qp.position());
      auto y = projection(x);
      dist += (x - y).two_norm2() * qp.weight() * geometry.integrationElement(qp.position());
    }
  }

  return sqrt(dist);
}

// Test integrated error in normal vectors
template <class Grid, class Projection>
typename Grid::ctype normal_error (const Grid& grid, const Projection& projection, int quad_order = 8)
{
  std::cout << "  normal_error..." << std::endl;
  using QuadProvider = Dune::QuadratureRules<typename Grid::ctype, 2>;
  typename Grid::ctype dist = 0;

  for (const auto& element : elements(grid.leafGridView()))
  {
    auto geometry = element.geometry();
    const auto& quadRule = QuadProvider::rule(element.type(), quad_order);
    for (const auto& qp : quadRule) {
      auto x = geometry.impl().normal(qp.position());
      auto y = projection.normal(geometry.global(qp.position()));
      dist += (x - y).two_norm2() * qp.weight() * geometry.integrationElement(qp.position());
    }
  }

  return std::sqrt(dist);
}

// Test integrated error in mean curvature
template <int order=2, class Grid, class Projection>
typename Grid::ctype curvature_error (const Grid& grid, const Projection& projection, int quad_order = 8)
{
  std::cout << "  curvature_error..." << std::endl;
  using QuadProvider = Dune::QuadratureRules<typename Grid::ctype, 2>;
  using LFE = LagrangeSimplexLocalFiniteElement<double,double,Grid::dimension,order+4>;

  typename Grid::ctype dist = 0;
  for (const auto& element : elements(grid.leafGridView()))
  {
    auto geometry = element.geometry().impl();
    auto lfe = LFE{};
    const auto& quadRule = QuadProvider::rule(element.type(), quad_order);
    for (const auto& qp : quadRule)
    {
      auto H = geometry.normalGradient(qp.position(), lfe);
      typename Grid::ctype trH = 0;
      for (std::size_t i = 0; i < H.N(); ++i)
        trH += H[i][i];

      auto trH_exact = projection.mean_curvature(geometry.global(qp.position()));
      dist += std::abs(trH/2 - trH_exact) * qp.weight() * geometry.integrationElement(qp.position());
    }
  }

  return dist;
}

// longest edge in the grid
template <class Grid>
typename Grid::ctype edge_length (const Grid& grid)
{
  typename Grid::ctype h = 0;
  for (const auto& e : edges(grid.hostGrid().leafGridView()))
    h = std::max(h, e.geometry().volume());

  return h;
}

} // end namespace Dune
