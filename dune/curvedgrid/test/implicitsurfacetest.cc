#include "config.h"

#include <iostream>

#if !HAVE_DUNE_FUNCTIONS
#error "Need dune-functions for the discretefunctiontest"
#endif

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/test/testsuite.hh>
#include <dune/curvedgrid/curvedgrid.hh>
#include <dune/curvedgrid/geometries/implicitsurface.hh>
#include <dune/curvedgrid/geometries/sphere.hh>
#include <dune/curvedgrid/gridfunctions/discretegridviewfunction.hh>
#include <dune/foamgrid/foamgrid.hh>
#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/grid/io/file/gmshreader.hh>

#include "convergence.hh"

using ctype = double;

const int order = 2;
const int quad_order = order+4;
const int num_levels = 5;


/// \brief Functor representing a sphere in dim-d
template <int dim>
class ImplicitSphere
{
  using T = ctype;
  using Domain = Dune::FieldVector<T,dim>;

  T radius_;

public:
  ImplicitSphere (T radius)
    : radius_(radius)
  {}

  T operator() (const Domain& x) const
  {
    return x.two_norm() - radius_;
  }

  friend auto derivative (const ImplicitSphere& sphere)
  {
    return [](const Domain& x)
    {
      return x / x.two_norm();
    };
  }
};

int main (int argc, char** argv)
{
  using namespace Dune;
  MPIHelper::instance(argc, argv);

  // using HostGrid = AlbertaGrid<2,3>;
  using HostGrid = FoamGrid<2,3,ctype>;
  std::unique_ptr<HostGrid> hostGrid = GmshReader<HostGrid>::read( DUNE_GRID_PATH "sphere.msh");

  auto sphere = SphereProjection<3>{1.0};
  auto sphereGridFct = implicitSurfaceGridFunction<HostGrid>(ImplicitSphere<3>{1.0});

  using Grid = CurvedGrid<HostGrid, decltype(sphereGridFct), true>;
  Grid grid(*hostGrid, sphereGridFct, order);

  std::vector<ctype> inf_errors, L2_errors, normal_errors, curvature_errors, edge_lengths;
  for (int i = 0; i < num_levels; ++i) {
    if (i > 0)
      grid.globalRefine(1);

    std::cout << "i = " << i << std::endl;
    inf_errors.push_back( inf_error(grid, sphere, quad_order) );
    L2_errors.push_back( L2_error(grid, sphere, quad_order) );
    normal_errors.push_back( normal_error(grid, sphere, quad_order) );
    curvature_errors.push_back( curvature_error<order>(grid, sphere, quad_order) );
    edge_lengths.push_back( edge_length(grid) );
  }


  using std::log;
  std::vector<ctype> eocInf(num_levels, 0), eocL2(num_levels, 0), eocNormal(num_levels, 0), eocCurvature(num_levels, 0);
  for (int i = 1; i < num_levels; ++i) {
    eocInf[i]       = log(inf_errors[i]/inf_errors[i-1]) / log(edge_lengths[i]/edge_lengths[i-1]);
    eocL2[i]        = log(L2_errors[i]/L2_errors[i-1]) / log(edge_lengths[i]/edge_lengths[i-1]);
    eocNormal[i]    = log(normal_errors[i]/normal_errors[i-1]) / log(edge_lengths[i]/edge_lengths[i-1]);
    eocCurvature[i] = log(curvature_errors[i]/curvature_errors[i-1]) / log(edge_lengths[i]/edge_lengths[i-1]);
  }

  TestSuite test;
  test.check(abs(eocInf.back()    - (order+1)) < 0.5, "|d|_L^inf != order+1");
  test.check(abs(eocL2.back()     - (order+1)) < 0.5, "|d|_L^2 != order+1");
  test.check(abs(eocNormal.back() - (order+0)) < 0.5, "|n - nh|_L^2 != order");
  test.check(abs(eocCurvature.back()   - (order-1)) < 0.5, "|H - Hh|_L^2 != order-1");

  auto print_line = [](auto i, const auto& data) {
    std::cout << i;
    for (const auto& d : data)
      std::cout << '\t' << "| " << d;
    std::cout << std::endl;
  };

  auto print_break = [] {
    std::cout.width(8 + 8*16);
    std::cout.fill('-');
    std::cout << '-' << std::endl;
    std::cout.fill(' ');
  };

  std::cout.setf(std::ios::scientific);
  print_line("level", std::vector<std::string>{"err_inf", "eoc_inf", "err_L2", "eoc_L2", "err_norm", "eoc_norm", "err_curv", "eoc_curv"});
  print_break();

  for (int i = 0; i < num_levels; ++i)
    print_line(i, std::vector<ctype>{inf_errors[i], eocInf[i], L2_errors[i], eocL2[i], normal_errors[i], eocNormal[i], curvature_errors[i], eocCurvature[i]});

  return test.exit();
}
