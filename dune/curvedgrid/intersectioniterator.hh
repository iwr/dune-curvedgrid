#ifndef DUNE_CURVEDGRID_INTERSECTIONITERATOR_HH
#define DUNE_CURVEDGRID_INTERSECTIONITERATOR_HH

#include <dune/curvedgrid/intersection.hh>

namespace Dune {
namespace Curved {

// IntersectionIterator
// --------------------

template <class Grid, class HostIntersectionIterator>
class IntersectionIterator
{
  using IntersectionImpl = Curved::Intersection<Grid, typename HostIntersectionIterator::Intersection>;

  using Traits = typename std::remove_const_t<Grid>::Traits;
  using GridFunction = typename Traits::GridFunction;
  using Element = typename Traits::template Codim<0>::Entity;
  using ElementGeometry = typename Traits::template Codim<0>::Geometry;

public:
  using Intersection = Dune::Intersection< Grid, IntersectionImpl>;

  IntersectionIterator() = default;

  IntersectionIterator (const Element& inside, const HostIntersectionIterator& hostIterator, int order)
    : hostIterator_(hostIterator)
    , gridFunction_(&inside.impl().gridFunction())
    , order_(order)
  {}

  bool equals (const IntersectionIterator& other) const
  {
    return (hostIterator_ == other.hostIterator_);
  }

  void increment ()
  {
    ++hostIterator_;
  }

  Intersection dereference () const
  {
    return IntersectionImpl(*hostIterator_, *gridFunction_, order_);
  }

private:
  HostIntersectionIterator hostIterator_;
  const GridFunction* gridFunction_ = nullptr;
  int order_ = -1;
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_INTERSECTIONITERATOR_HH
