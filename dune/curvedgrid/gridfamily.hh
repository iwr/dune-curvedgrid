#ifndef DUNE_CURVEDGRID_GRIDFAMILY_HH
#define DUNE_CURVEDGRID_GRIDFAMILY_HH

#include <type_traits>

#include <dune/common/referencehelper.hh>
#include <dune/common/std/type_traits.hh>
#include <dune/curvedgrid/capabilities.hh>
#include <dune/curvedgrid/entity.hh>
#include <dune/curvedgrid/entityseed.hh>
#include <dune/curvedgrid/geometry.hh>
#include <dune/curvedgrid/gridview.hh>
#include <dune/curvedgrid/intersection.hh>
#include <dune/curvedgrid/intersectioniterator.hh>
#include <dune/curvedgrid/iterator.hh>
#include <dune/curvedgrid/idset.hh>
#include <dune/curvedgrid/indexsets.hh>
#include <dune/curvedgrid/localgeometrywrapper.hh>
#include <dune/curvedgrid/gridfunctions/gridfunction.hh>
#include <dune/grid/common/grid.hh>

namespace Dune {

/** \brief namespace containing the implementations of CurvedGrid
 *  \ingroup CurvedGeo
 */
namespace Curved {

template <class GF>
struct DimRange
{
  using EntitySet = typename GF::EntitySet;
  using Range = std::invoke_result_t<GF,typename EntitySet::GlobalCoordinate>;
  using RawRange = std::decay_t<Range>;
  static const int value = RawRange::size();
};

// GridFamily
// ----------

template <class HG, class GF, bool useInterpolation>
struct GridFamily
{
  struct Traits
  {
    using Grid = CurvedGrid<HG,GF,useInterpolation>;
    using HostGrid = std::remove_const_t<HG>;

    using GridFunction = ResolveReference_t<GF>;
    using LocalFunction
      = std::decay_t<decltype(localFunction(std::declval<const GridFunction&>()))>;

    using ctype = typename HostGrid::ctype;

    static const int dimension = HostGrid::dimension;
    static const int dimensionworld = DimRange<GridFunction>::value;

    using LeafIntersection
      = Dune::Intersection<const Grid, Curved::Intersection<const Grid, typename HostGrid::LeafIntersection>>;
    using LevelIntersection
      = Dune::Intersection<const Grid, Curved::Intersection<const Grid, typename HostGrid::LevelIntersection>>;

    using LeafIntersectionIterator
      = Dune::IntersectionIterator<const Grid,
          Curved::IntersectionIterator<const Grid, typename HostGrid::LeafIntersectionIterator>,
          Curved::Intersection<const Grid, typename HostGrid::LeafIntersection> >;
    using LevelIntersectionIterator
      = Dune::IntersectionIterator<const Grid,
          Curved::IntersectionIterator<const Grid, typename HostGrid::LevelIntersectionIterator>,
          Curved::Intersection<const Grid, typename HostGrid::LevelIntersection> >;

    using HierarchicIterator
      = Dune::EntityIterator<0, const Grid, Curved::HierarchicIterator<const Grid>>;

    template <int codim>
    struct Codim
    {
      using LocalGeometry = typename HostGrid::template Codim<codim>::LocalGeometry;

      using LocalTransformation = std::conditional_t<(codim == 0),
        DefaultLocalGeometry<ctype, dimension, dimension>,
        Curved::LocalGeometryWrapper<LocalGeometry>>;

      template <int mydim, int cdim, class GridImpl>
      using GeometryImplTemplate
        = Curved::Geometry<ctype, mydim, cdim, LocalFunction, LocalTransformation, useInterpolation>;

      // geometry types
      using GeometryImpl = GeometryImplTemplate<dimension-codim, dimensionworld, const Grid>;
      using Geometry = Dune::Geometry<dimension-codim, dimensionworld, const Grid, GeometryImplTemplate>;

      // entity types
      using EntityImpl = Curved::Entity<codim, dimension, const Grid>;
      using Entity = Dune::Entity<codim, dimension, const Grid, Curved::Entity>;

      using EntitySeed = Dune::EntitySeed<const Grid, Curved::EntitySeed<codim, const Grid> >;

      template <PartitionIteratorType pitype>
      struct Partition
      {
        using LeafIteratorImp
          = Curved::Iterator<typename HostGrid::LeafGridView, codim, pitype, const Grid>;
        using LeafIterator = Dune::EntityIterator<codim, const Grid, LeafIteratorImp>;

        using LevelIteratorImp
          = Curved::Iterator<typename HostGrid::LevelGridView, codim, pitype, const Grid>;
        using LevelIterator = Dune::EntityIterator<codim, const Grid, LevelIteratorImp>;
      };

      using LeafIterator = typename Partition< All_Partition >::LeafIterator;
      using LevelIterator = typename Partition< All_Partition >::LevelIterator;
    };

    // index-sets
    using LeafIndexSet = Curved::IndexSet<const Grid, typename HostGrid::Traits::LeafIndexSet>;
    using LevelIndexSet = Curved::IndexSet<const Grid, typename HostGrid::Traits::LevelIndexSet>;

    // id-sets
    using GlobalIdSet = Curved::IdSet<const Grid, typename HostGrid::Traits::GlobalIdSet>;
    using LocalIdSet = Curved::IdSet<const Grid, typename HostGrid::Traits::LocalIdSet>;

    using Communication = typename HostGrid::Traits::Communication;

    // grid views
    using LeafGridView
      = Dune::GridView<Curved::GridViewTraits<typename HostGrid::LeafGridView, GF, useInterpolation>>;
    using LevelGridView
      = Dune::GridView<Curved::GridViewTraits<typename HostGrid::LevelGridView, GF, useInterpolation>>;
  };
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_GRIDFAMILY_HH
