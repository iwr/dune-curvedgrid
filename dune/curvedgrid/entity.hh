#ifndef DUNE_CURVEDGRID_ENTITY_HH
#define DUNE_CURVEDGRID_ENTITY_HH

#include <optional>

#include <dune/curvedgrid/geometry.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/grid.hh>

namespace Dune {
namespace Curved {

// Internal Forward Declarations
// -----------------------------

/** \class EntityBase
 *  \brief actual implementation of the entity
 *  \ingroup CurvedGeo
 *
 *  \tparam  codim  codimension of the entity
 *  \tparam  Grid   CurvedGrid, this entity belongs to
 */
template <int codim, class Grid>
class EntityBase;

/** \class Entity
 *  \brief DUNE-conform implementation of the entity
 *  \ingroup CurvedGeo
 *
 *  This class merely changes the template parameters of the entity to make
 *  DUNE happy. The actual implementation of the entity can be found in
 *  EntityBase.
 *
 *  \tparam  codim  codimension of the entity
 *  \tparam  dim    dimension of the Grid (redundant information)
 *  \tparam  Grid   CurvedGrid, this entity belongs to
 */
template <int codim, int dim, class Grid>
class Entity;



// External Forward Declarations
// -----------------------------

template <class Grid>
class HierarchicIterator;

template <class Grid, class HostIntersectionIterator>
class IntersectionIterator;


template <int codim, class G>
class EntityBase
{
  using Traits = typename std::remove_const_t<G>::Traits;

public:
  /** \name Attributes
   *  \{ */

  /// \brief codimensioon of the entity
  static const int codimension = codim;
  /// \brief dimension of the grid
  static const int dimension = Traits::dimension;
  /// \brief dimension of the entity
  static const int mydimension = dimension - codimension;
  /// \brief dimension of the world
  static const int dimensionworld = Traits::dimensionworld;

  /** \} */

  /** \name Types Required by DUNE
   *  \{ */

  /// \brief coordinate type of the grid
  using ctype = typename Traits::ctype;

  /** \} */

public:
  /** \name Host Types
   *  \{ */

  /// \brief type of corresponding entity seed
  using EntitySeed = typename Traits::template Codim<codimension>::EntitySeed;

  /** \} */

public:
  using Grid = typename Traits::Grid;
  using GridFunction = typename Traits::GridFunction;

  /// \brief type of the host grid
  using HostGrid = typename Traits::HostGrid;

  /// \brief type of corresponding host entity
  using HostEntity = typename HostGrid::template Codim<codim>::Entity;

  /// \brief type of host elements, i.e., of host entities of codimension 0
  using HostElement = typename HostGrid::template Codim<0>::Entity;

public:
  EntityBase () = default;

  /// \brief Construct the entity from an entity seed
  EntityBase (const Grid& grid, const EntitySeed& seed)
    : hostEntity_(grid.hostGrid().entity(seed.impl().hostEntitySeed()))
    , gridFunction_(&grid.gridFunction())
    , order_(grid.order())
  {}

  /// \brief construct the entity from a subentity of a host-entity
  EntityBase (const GridFunction& gridFunction, const HostElement& hostElement, int i, int order)
    : hostEntity_(hostElement.template subEntity<codim>(i))
    , gridFunction_(&gridFunction)
    , order_(order)
  {}

  /// \brief construct the entity from a host-entity
  EntityBase (const GridFunction& gridFunction, const HostEntity& hostEntity, int order)
    : hostEntity_(hostEntity)
    , gridFunction_(&gridFunction)
    , order_(order)
  {}

  /// \brief construct the entity from a host-entity
  EntityBase (const GridFunction& gridFunction, HostEntity&& hostEntity, int order)
    : hostEntity_(std::move(hostEntity))
    , gridFunction_(&gridFunction)
    , order_(order)
  {}

  /// \brief compare two entities
  bool equals (const EntityBase& other) const
  {
    return hostEntity_ == other.hostEntity_;
  }

public:
  /** \name Methods Shared by Entities of All Codimensions
   *  \{ */

  /// \brief obtain the name of the corresponding reference element
  GeometryType type () const
  {
    return hostEntity().type();
  }

  /// \brief obtain the level of this entity
  int level () const
  {
    return hostEntity().level();
  }

  /// \brief obtain the partition type of this entity
  PartitionType partitionType () const
  {
    return hostEntity().partitionType();
  }

  /// \brief obtain number of sub-entities of the current entity
  unsigned int subEntities (unsigned int cc) const
  {
    return hostEntity().subEntities(cc);
  }

  /// \brief return EntitySeed of host grid entity
  EntitySeed seed () const
  {
    return typename EntitySeed::Implementation(hostEntity().seed());
  }


  /** \name Methods Supporting the Grid Implementation
   *  \{ */

  const GridFunction& gridFunction () const
  {
    return *gridFunction_;
  }

  /// \brief return the wrapped host-entity
  const HostEntity& hostEntity () const
  {
    return hostEntity_;
  }

  int order () const
  {
    return order_;
  }

  /** \} */

private:
  HostEntity hostEntity_;
  const GridFunction* gridFunction_ = nullptr;
  int order_ = -1;
};


template <int codim, class G>
class EntityBaseGeometry
    : public EntityBase<codim, G>
{
  using Super = EntityBase<codim, G>;
  using Traits = typename std::remove_const_t<G>::Traits;

  /// \brief type of host elements, i.e., of host entities of codimension 0
  using HostElement = typename Traits::HostGrid::template Codim<0>::Entity;
  using GridFunction = typename Super::GridFunction;

public:
  /// \brief type of corresponding geometry
  using Geometry = typename Traits::template Codim<codim>::Geometry;
  using ctype = typename Geometry::ctype;

public:
  using Super::Super;

  /// \brief Construct the entity from a subentity of a host-entity
  EntityBaseGeometry (const GridFunction& gridFunction, const HostElement& hostElement, int i, int order)
    : Super(gridFunction, hostElement, i, order)
    , hostElement_(hostElement)
    , subEntity_(i)
  {}

  /// \brief Obtain the geometry of this entity
  /**
   * The geometry can only be constructed of the entity is constructed as a sub-entity
   * of a grid elements. This is, because the geometry is parametrized by a local function
   * that must be bound to a grid element.
   *
   * NOTE: This only works if the grid is twist-free. Otherwise, the local-geometry of the
   * reference element might have a different orientation than the geometry of the actual
   * entity in the grid.
   **/
  Geometry geometry () const
  {
    if (!geo_) {
      if (hostElement_) {
        auto localFct = localFunction(Super::gridFunction());
        localFct.bind(*hostElement_);

        auto refElem = referenceElement<ctype, Super::dimension>(hostElement_->type());
        auto localGeometry = refElem.template geometry<codim>(subEntity_);
        geo_.emplace(Super::type(), Super::order(), std::move(localFct), localGeometry);
      }
      else {
        DUNE_THROW(Dune::NotImplemented, "Geometry of entities of codim!=0 not implemented");
      }
    }

    return Geometry(*geo_);
  }

private:
  std::optional<HostElement> hostElement_;
  int subEntity_ = -1;

  using GeometryImpl = typename Traits::template Codim<codim>::GeometryImpl;
  mutable std::optional<GeometryImpl> geo_;
};


template <class G>
class EntityBaseGeometry<0, G>
    : public EntityBase<0, G>
{
  using Super = EntityBase<0, G>;
  using Traits = typename std::remove_const_t<G>::Traits;

public:
  /// \brief type of corresponding geometry
  using Geometry = typename Traits::template Codim<0>::Geometry;

  /// \brief type of corresponding local geometry
  using LocalGeometry = typename Traits::template Codim<0>::LocalGeometry;

public:
  using Super::Super;

  /// \brief obtain the geometry of this entity
  /**
   * The geometry is constructed by parametrization over the host geometry.
   * Internally, the local function is used to construct this parametrization
   * either implicitly, by first interpolating the local function into a local basis
   * or explicitly as reinterpreting the local function directly as geometry.
   *
   * NOTE: The geometry expects a LocalGeometry parametrization for the entity in the
   * grid element. Since this specialization is for grid elements, this parametrization
   * mapping is just the identity. This is implemented by using the DefaultLocalGeometry
   * that simply forwards the input arguments of all the functions or returns an identity
   * object.
   */
  Geometry geometry () const
  {
    if (!geo_) {
      auto localFct = localFunction(Super::gridFunction());
      localFct.bind(Super::hostEntity());
      auto fakeDefaultGeometry = Dune::DefaultLocalGeometry<typename Super::ctype, Super::mydimension, Super::mydimension>{};
      geo_.emplace(Super::type(), Super::order(), std::move(localFct), fakeDefaultGeometry);
    }

    return Geometry(*geo_);
  }

private:
  using GeometryImpl = typename Traits::template Codim<0>::GeometryImpl;
  mutable std::optional<GeometryImpl> geo_;
};


// Entity
// ------

template <int codim, int dim, class G>
class Entity
    : public EntityBaseGeometry<codim, G>
{
  using Super = EntityBaseGeometry<codim, G>;

public:
  // import constructors from base class
  using Super::Super;
};


// Entity for codimension 0
// ------------------------

template <int dim, class G>
class Entity<0, dim, G>
    : public EntityBaseGeometry<0, G>
{
  using Super = EntityBaseGeometry<0, G>;

  using Traits = typename std::remove_const_t<G>::Traits;
  using Grid = typename Traits::Grid;
  using GridFunction = typename Grid::GridFunction;
  using HostGrid = typename Traits::HostGrid;

public:

  /** \name Types Required by DUNE
   *  \{ */

  /// \brief type of corresponding local geometry
  using LocalGeometry = typename Traits::template Codim<0>::LocalGeometry;

  /// \brief facade type for entities
  using EntityFacade = Dune::Entity<0, dim, G, Dune::Curved::Entity>;

  /// \brief type of hierarchic iterator
  using HierarchicIterator = typename Traits::HierarchicIterator;

  /// \brief type of leaf intersection iterator
  using LeafIntersectionIterator = typename Traits::LeafIntersectionIterator;

  /// \brief type of level intersection iterator
  using LevelIntersectionIterator = typename Traits::LevelIntersectionIterator;

  /** \} */

  // import constructors from base class
  using Super::Super;

  template <int codim>
  typename Grid::template Codim<codim>::Entity subEntity (int i) const
  {
    using EntityImpl = typename Traits::template Codim<codim>::EntityImpl;
    return EntityImpl(Super::gridFunction(), Super::hostEntity(), i, Super::order());
  }

  LevelIntersectionIterator ilevelbegin () const
  {
    using IteratorImpl = Curved::IntersectionIterator<Grid, typename HostGrid::LevelIntersectionIterator>;
    return IteratorImpl(*this, Super::hostEntity().ilevelbegin(), Super::order());
  }

  LevelIntersectionIterator ilevelend () const
  {
    using IteratorImpl = Curved::IntersectionIterator<Grid, typename HostGrid::LevelIntersectionIterator>;
    return IteratorImpl(*this, Super::hostEntity().ilevelend(), Super::order());
  }

  LeafIntersectionIterator ileafbegin () const
  {
    using IteratorImpl = Curved::IntersectionIterator<Grid, typename HostGrid::LeafIntersectionIterator>;
    return IteratorImpl(*this, Super::hostEntity().ileafbegin(), Super::order());
  }

  LeafIntersectionIterator ileafend () const
  {
    using IteratorImpl = Curved::IntersectionIterator<Grid, typename HostGrid::LeafIntersectionIterator>;
    return IteratorImpl(*this, Super::hostEntity().ileafend(), Super::order());
  }

  bool hasBoundaryIntersections () const
  {
    return Super::hostEntity().hasBoundaryIntersections();
  }

  bool isLeaf () const
  {
    return Super::hostEntity().isLeaf();
  }

  EntityFacade father () const
  {
    return Entity(Super::gridFunction(), Super::hostEntity().father(), Super::order());
  }

  bool hasFather () const
  {
    return Super::hostEntity().hasFather();
  }

  LocalGeometry geometryInFather () const
  {
    return Super::hostEntity().geometryInFather();
  }

  HierarchicIterator hbegin (int maxLevel) const
  {
    using IteratorImpl = Curved::HierarchicIterator<G>;
    return IteratorImpl(Super::gridFunction(), Super::hostEntity().hbegin(maxLevel), Super::order());
  }

  HierarchicIterator hend (int maxLevel) const
  {
    using IteratorImpl = Curved::HierarchicIterator<G>;
    return IteratorImpl(Super::gridFunction(), Super::hostEntity().hend(maxLevel), Super::order());
  }

  bool isRegular () const
  {
    return Super::hostEntity().isRegular();
  }

  bool isNew () const
  {
    return Super::hostEntity().isNew();
  }

  bool mightVanish () const
  {
    return Super::hostEntity().mightVanish();
  }
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_ENTITY_HH
