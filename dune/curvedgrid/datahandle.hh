#ifndef DUNE_CURVEDGRID_DATAHANDLE_HH
#define DUNE_CURVEDGRID_DATAHANDLE_HH

#include <dune/common/typetraits.hh>
#include <dune/curvedgrid/entity.hh>
#include <dune/grid/common/datahandleif.hh>
#include <dune/grid/common/grid.hh>

namespace Dune {
namespace Curved {

// CommDataHandle
// --------------

template <class Grid, class WrappedHandle>
class CommDataHandle
    : public CommDataHandleIF<CommDataHandle<Grid, WrappedHandle>, typename WrappedHandle::DataType>
{
  using Traits = typename std::remove_const_t<Grid>::Traits;

public:
  CommDataHandle (const Grid& grid, WrappedHandle& handle)
    : grid_(grid)
    , wrappedHandle_(handle)
  {}

  bool contains (int dim, int codim) const
  {
    const bool contains = wrappedHandle_.contains(dim, codim);
    if (contains)
      assertHostEntity(dim, codim);
    return contains;
  }

  bool fixedSize (int dim, int codim) const
  {
    return wrappedHandle_.fixedSize(dim, codim);
  }

  template <class HostEntity>
  std::size_t size (const HostEntity& hostEntity) const
  {
    using Entity = typename Grid::Traits::template Codim<HostEntity::codimension>::Entity;
    using EntityImpl = typename Grid::Traits::template Codim<HostEntity::codimension>::EntityImpl;
    Entity entity(EntityImpl(grid_.gridFunction(), hostEntity, grid_.order()));
    return wrappedHandle_.size(entity);
  }

  template <class MessageBuffer, class HostEntity>
  void gather (MessageBuffer& buffer, const HostEntity& hostEntity) const
  {
    using Entity = typename Grid::Traits::template Codim<HostEntity::codimension>::Entity;
    using EntityImpl = typename Grid::Traits::template Codim<HostEntity::codimension>::EntityImpl;
    Entity entity(EntityImpl(grid_.gridFunction(), hostEntity, grid_.order()));
    wrappedHandle_.gather(buffer, entity);
  }

  template <class MessageBuffer, class HostEntity>
  void scatter (MessageBuffer& buffer, const HostEntity& hostEntity, std::size_t size)
  {
    using Entity = typename Grid::Traits::template Codim< HostEntity::codimension >::Entity;
    using EntityImpl = typename Grid::Traits::template Codim< HostEntity::codimension >::EntityImpl;
    Entity entity(EntityImpl(grid_.gridFunction(), hostEntity, grid_.order()));
    wrappedHandle_.scatter(buffer, entity, size);
  }

private:
  static void assertHostEntity (int dim, int codim)
  {
    if (!Capabilities::CodimCache<Grid>::hasHostEntity(codim))
      DUNE_THROW(NotImplemented, "Host grid has no entities for codimension " << codim << ".");
  }

private:
  const Grid& grid_;
  WrappedHandle& wrappedHandle_;
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_DATAHANDLE_HH
