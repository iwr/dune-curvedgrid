#ifndef DUNE_CURVEDGRID_VERTEXCACHE_HH
#define DUNE_CURVEDGRID_VERTEXCACHE_HH

#include <dune/common/fvector.hh>

#define VERTEXCACHE_DEBUG_MODE 0

namespace Dune {

template <class T, class FT>
class VertexCache
{
  using Self = VertexCache<T, FT>;
  using Vertex = FieldVector<FT, 3>;

  const unsigned partition_threshold = 64;
  const unsigned depth_threshold = 7;
  const FT epsilon = std::sqrt(std::numeric_limits<FT>::epsilon());

  unsigned depth_;
  Vertex cbeg_, cmid_, cend_;                   //< coordinates
  std::vector<VertexCache<T, FT>> partitions_;
  std::vector<std::pair<Vertex, T>> entries_;   //< VertexCache behaves like a map<Vertex, T>

public:
  VertexCache (const Vertex& cbeg, const Vertex& cend, unsigned depth = 0)
    : depth_(depth)
    , cbeg_(cbeg)
    , cmid_((cbeg + cend) / 2.0)
    , cend_(cend)
  {}

  VertexCache (const Self& other)
    : depth_(other.depth_)
    , cbeg_(other.cbeg_)
    , cmid_(other.cmid_)
    , cend_(other.cend_)
    , partitions_(other.partitions_)
    , entries_(other.entries_)
  {}

  VertexCache (Self&& other)
    : depth_(other.depth_)
    , cbeg_(other.cbeg_)
    , cmid_(other.cmid_)
    , cend_(other.cend_)
    , partitions_(std::move(other.partitions_))
    , entries_(std::move(other.entries_))
  {}

  Self& operator= (const Self&) = delete;
  Self& operator= (Self&&) = delete;

  void clear ()
  {
    entries_.clear();
    partitions_.clear();
  }

  void split ()
  {
    //p0: (beg, beg, beg) - (mid, mid, mid)
    //p1: (mid, beg, beg) - (end, mid, mid)
    //p2: (beg, mid, beg) - (mid, end, mid)
    //p3: (mid, mid, beg) - (end, end, mid)
    //p4: (beg, beg, mid) - (mid, mid, end)
    //p5: (mid, beg, mid) - (end, mid, end)
    //p6: (beg, mid, mid) - (mid, end, end)
    //p7: (mid, mid, mid) - (end, end, end)
    partitions_.reserve(8);
    Vertex c0 = cbeg_, c1 = cmid_;
    partitions_.emplace_back(c0, c1, depth_ + 1); //p0
    c0[0] = cmid_[0];
    c1[0] = cend_[0];
    partitions_.emplace_back(c0, c1, depth_ + 1); //p1
    c0[0] = cbeg_[0];
    c1[0] = cmid_[0];
    c0[1] = cmid_[1];
    c1[1] = cend_[1];
    partitions_.emplace_back(c0, c1, depth_ + 1); //p2
    c0[0] = cmid_[0];
    c1[0] = cend_[0];
    partitions_.emplace_back(c0, c1, depth_ + 1); //p3
    c0[0] = cbeg_[0];
    c1[0] = cmid_[0];
    c0[1] = cbeg_[1];
    c1[1] = cmid_[1];
    c0[2] = cmid_[2];
    c1[2] = cend_[2];
    partitions_.emplace_back(c0, c1, depth_ + 1); //p4
    c0[0] = cmid_[0];
    c1[0] = cend_[0];
    partitions_.emplace_back(c0, c1, depth_ + 1); //p5
    c0[0] = cbeg_[0];
    c1[0] = cmid_[0];
    c0[1] = cmid_[1];
    c1[1] = cend_[1];
    partitions_.emplace_back(c0, c1, depth_ + 1); //p6
    partitions_.emplace_back(cmid_, cend_, depth_ + 1); //p7

    for(auto it = entries_.begin(), end = entries_.end(); it != end; ++it) {
      insert(it->first, it->second);
    }
    entries_.clear();
  }

  void insert (const Vertex& v, const T& data)
  {
    if (partitions_.empty()) {
      entries_.push_back(std::pair<Vertex, T>(v, data));
      if (entries_.size() > partition_threshold && depth_ < depth_threshold) split();
    } else {
      if (v[2] <= cmid_[2]) { //p0, p1, p2, p3
        if (v[1] <= cmid_[1]) { //p0, p1
          if (v[0] <= cmid_[0]) partitions_[0].insert(v, data);
          else partitions_[1].insert(v, data);
        } else { //p2, p3
          if(v[0] <= cmid_[0]) partitions_[2].insert(v, data);
          else partitions_[3].insert(v, data);
        }
      } else { //p4, p5, p6, p7
        if (v[1] <= cmid_[1]) { //p4, p5
          if (v[0] <= cmid_[0]) partitions_[4].insert(v, data);
          else partitions_[5].insert(v, data);
        } else { //p6, p7
          if(v[0] <= cmid_[0]) partitions_[6].insert(v, data);
          else partitions_[7].insert(v, data);
        }
      }
    }
  }

  bool find (const Vertex& v, T& result)
  {
    if (partitions_.empty()) {
      for (auto it = entries_.begin(), end = entries_.end(); it != end; ++it) {
        Vertex &t = it->first;
        if (fabs(t[0] - v[0]) <= epsilon && fabs(t[1] - v[1]) <= epsilon
                                         && fabs(t[2] - v[2]) <= epsilon) {
          result = it->second;
          return true;
        }
      }
    } else {
      if (v[2] - epsilon <= cmid_[2]) { //p0, p1, p2, p3
        if (v[1] - epsilon <= cmid_[1]) { //p0, p1
          if (v[0] - epsilon <= cmid_[0]) if (partitions_[0].find(v, result)) return true;
          if (v[0] + epsilon >= cmid_[0]) if (partitions_[1].find(v, result)) return true;
        }
        if (v[1] + epsilon >= cmid_[1]) { //p2, p3
          if (v[0] - epsilon <= cmid_[0]) if (partitions_[2].find(v, result)) return true;
          if (v[0] + epsilon >= cmid_[0]) if (partitions_[3].find(v, result)) return true;
        }
      }
      if (v[2] + epsilon >= cmid_[2]) { //p4, p5, p6, p7
        if (v[1] - epsilon <= cmid_[1]) { //p4, p5
          if (v[0] - epsilon <= cmid_[0]) if (partitions_[4].find(v, result)) return true;
          if( v[0] + epsilon >= cmid_[0]) if (partitions_[5].find(v, result)) return true;
        }
        if (v[1] + epsilon >= cmid_[1]) { //p6, p7
          if (v[0] - epsilon <= cmid_[0]) if (partitions_[6].find(v, result)) return true;
          if (v[0] + epsilon >= cmid_[0]) if (partitions_[7].find(v, result)) return true;
        }
      }
    }
    return false;
  }

#if VERTEXCACHE_DEBUG_MODE != 0
  void debug_output_statistics () const
  {
    std::cout << "\nVertexCache statistics:" << std::endl;
    size_t node_count = 0, leaf_count = 0, crowded_leaf_count = 0, entry_count = 0,
                                                                max_leaf_entries = 0;
    debug_statistics_first_run(node_count, leaf_count, crowded_leaf_count,
                                            entry_count, max_leaf_entries);
    double avg = (double)entry_count / leaf_count;
    double std_dev = 0.0;
    if(entry_count > 0){
      debug_statistics_second_run(avg, entry_count, std_dev);
      std_dev = sqrt(std_dev);
    }
    std::cout << "  node-count:         " << node_count << std::endl;
    std::cout << "  leaf-count:         " << leaf_count
              << " (" << (double)leaf_count / node_count * 100.0 << "%)" << std::endl;
    std::cout << "  crowded leaf-count: " << crowded_leaf_count
              << " (" << (double)crowded_leaf_count / leaf_count * 100.0 << "%)" << std::endl;
    std::cout << "  entry-count:        " << entry_count << std::endl;
    std::cout << "  max. leaf-entries:  " << max_leaf_entries << std::endl;
    std::cout << "  avg. leaf-entries:  " << avg << std::endl;
    std::cout << "  std. deviation:     " << std_dev << std::endl;
  }

  void debug_statistics_first_run (size_t &node_count, size_t &leaf_count,
                size_t &crowded_leaf_count, size_t &entry_count, size_t &max_leaf_entries) const
  {
    ++node_count;
    if(partitions_.empty()){
      ++leaf_count;
      entry_count += entries_.size();
      max_leaf_entries = std::max(max_leaf_entries, entries_.size());
      if(entries_.size() > partition_threshold) ++crowded_leaf_count;
    }else{
      for(int i = 0; i < 8; ++i){
        partitions_[i].debug_statistics_first_run(node_count, leaf_count,
                          crowded_leaf_count, entry_count, max_leaf_entries);
      }
    }
  }

  void debug_statistics_second_run (double avg, size_t entry_count, double &std_dev_sum) const
  {
    if (partitions_.empty()) {
      double deviation = (double)entries_.size() - avg;
      std_dev_sum += (deviation * deviation) / entry_count;
    } else {
      for (int i = 0; i < 8; ++i) {
        partitions_[i].debug_statistics_second_run(avg, entry_count, std_dev_sum);
      }
    }
  }

  void debug_output (int indentation=0) const
  {
    std::string id_str = std::string(indentation, ' ');
    if (indentation == 0) std::cout << "\nVertexCache:" << std::endl;
    if (partitions_.empty()) {
      for (auto it = entries_.begin(), end = entries_.end(); it != end; ++it) {
        std::cout << id_str << it->first << " -> " << it->second << std::endl;
      }
    } else {
      for (int i = 0; i < 8; ++i) {
        std::cout << id_str << "p" << i << "{" << std::endl;
        partitions_[i].debug_output(indentation + 2);
        std::cout << id_str << "}" << std::endl;
      }
    }
  }
#endif
};

} // end namespace Dune

#endif // DUNE_CURVEDGRID_VERTEXCACHE_HH
