#ifndef DUNE_CURVEDGRID_KDTREE_HH
#define DUNE_CURVEDGRID_KDTREE_HH

#include <cstdlib>
#include <iostream>

#include <nanoflann.hpp>

#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>

namespace Dune {

/// \brief A simple vector-of-vectors adaptor for nanoflann, without duplicating the storage.
template <class T>
class KDTree
{
	using WorldVector = FieldVector<T,3>;

  /// \brief Type of the container to store the points
  using VectorOfVectorsType = std::vector<WorldVector>;

  /// \brief The type of the point coordinates (typically, double or float).
  using num_t = T;

  /// \brief The distance metric to use: nanoflann::metric_L1, nanoflann::metric_L2, nanoflann::metric_L2_Simple, etc.
  using Distance = nanoflann::metric_L2;

  /// \brief The type for indices in the KD-tree index (typically, size_t of int)
  using IndexType = std::size_t;

  /// \brief If set to >0, it specifies a compile-time fixed dimensionality for the points in the data set, allowing more compiler optimizations.
  static constexpr int DIM = WorldVector::size();

private:
	using self_t = KDTree;
	using metric_t = typename Distance::template traits<num_t,self_t>::distance_t;
	using index_t = nanoflann::KDTreeSingleIndexAdaptor<metric_t,self_t,DIM,IndexType>;

private:
  /// \brief The kd-tree index for the user to call its methods as usual with any other FLANN index.
	std::unique_ptr<index_t> index_;

  /// \brief The container storing all the points
	const VectorOfVectorsType* data_;

  /// \brief bounding box
  WorldVector minCorner_;
  WorldVector maxCorner_;

public:
	/// Constructor: takes a const ref to the vector of vectors object with the data points
	KDTree (const VectorOfVectorsType& mat, const WorldVector& minCorner, const WorldVector& maxCorner, int leaf_max_size = 10)
    : data_(&mat)
    , minCorner_(minCorner)
    , maxCorner_(maxCorner)
	{
		index_ = std::make_unique<index_t>(DIM, *this /* adaptor */, nanoflann::KDTreeSingleIndexAdaptorParams(leaf_max_size));
	}

  /// \brief Must be called before the index can be used
  void update ()
  {
#ifndef NDEBUG
    Timer t;
#endif

		assert(data_->size() != 0 && (*data_)[0].size() != 0);
		index_->buildIndex();

#ifndef NDEBUG
    std::cout << "update kdtree needed " << t.elapsed() << " seconds" << std::endl;
#endif
  }

  void update (const VectorOfVectorsType& mat)
	{
		data_ = &mat;
	}

	/// \brief Query for the numClosest closest points to a given queryPoint
	void query (const WorldVector& queryPoint, std::size_t numClosest, std::vector<IndexType>& outIndices, std::vector<num_t>& outDistancesSq) const
	{
    nanoflann::KNNResultSet<num_t,IndexType> resultSet(numClosest);
		resultSet.init(outIndices.data(), outDistancesSq.data());
		index_->findNeighbors(resultSet, queryPoint.data(), nanoflann::SearchParams());
	}

  /// \brief Search for all point in a given radius around the queryPoint. Store the indices and the squared distance in the `matches` output vector
	void query (const WorldVector& queryPoint, num_t radius, std::vector<std::pair<IndexType,num_t>>& matches) const
	{
		index_->radiusSearch(queryPoint.data(), radius, matches, nanoflann::SearchParams());
	}

	/** @name Interface expected by KDTreeSingleIndexAdaptor
	  * @{ */

	const self_t& derived () const { return *this; }

	self_t& derived () { return *this; }

	// Must return the number of data points
	std::size_t kdtree_get_point_count () const
  {
		return data_->size();
	}

	// Returns the dim'th component of the idx'th point in the class:
	num_t kdtree_get_pt (std::size_t idx, std::size_t dim) const
  {
		return (*data_)[idx][dim];
	}

	// Optional bounding-box computation: return false to default to a standard bbox computation loop.
	//   Return true if the BBOX was already computed by the class and returned in "bb" so it can be avoided to redo it again.
	//   Look at bb.size() to find out the expected dimensionality (e.g. 2 or 3 for point clouds)
	template <class BBOX>
	bool kdtree_get_bbox (BBOX& bb) const
  {
    for (int i = 0; i < DIM; ++i) {
      bb[i].low = minCorner_[i];
      bb[i].high = maxCorner_[i];
    }
		return true;
	}

	/** @} */

}; // end of KDTree

} // end namespace Dune

#endif // DUNE_CURVEDGRID_KDTREE_HH
