#ifndef DUNE_CURVEDGRID_ENTITYSEED_HH
#define DUNE_CURVEDGRID_ENTITYSEED_HH

#include <dune/common/typetraits.hh>
#include <dune/curvedgrid/capabilities.hh>
#include <dune/grid/common/entityseed.hh>

namespace Dune {
namespace Curved {

// EntitySeed
// ----------

template <int codim, class Grid>
class EntitySeed
{
  using Traits = typename std::remove_const_t<Grid>::Traits;
  using HostGrid = typename Traits::HostGrid;
  using HostEntitySeed = typename HostGrid::template Codim<codim>::EntitySeed;

public:
  static const int codimension = codim;

  /// \brief default construct an invalid entity seed
  EntitySeed () = default;

  /// \brief construct the entity-seed from a host-entity seed
  explicit EntitySeed (const HostEntitySeed& hostEntitySeed)
    : hostEntitySeed_(hostEntitySeed)
  {}

  /// \brief check whether the EntitySeed refers to a valid Entity
  bool isValid () const
  {
    return hostEntitySeed_.isValid();
  }

  /// \brief return the entity seed of the host-entity
  const HostEntitySeed& hostEntitySeed () const
  {
    return hostEntitySeed_;
  }

private:
  HostEntitySeed hostEntitySeed_ = {};
};

} // end namespace Curved
} // end namespace Dune

#endif // DUNE_CURVEDGRID_ENTITYSEED_HH
