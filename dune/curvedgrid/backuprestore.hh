#ifndef DUNE_CURVEDGRID_BACKUPRESTORE_HH
#define DUNE_CURVEDGRID_BACKUPRESTORE_HH

#include <dune/curvedgrid/capabilities.hh>
#include <dune/grid/common/backuprestore.hh>


namespace Dune {
namespace Curved {

  // BackupRestoreFacilities
  // -----------------------

  template <class Grid, bool hasBackupRestoreFacilities = Capabilities::hasBackupRestoreFacilities<Grid>::v>
  class BackupRestoreFacilities
  {};

  template <class Grid>
  class BackupRestoreFacilities<Grid, true>
  {
    using Self = BackupRestoreFacilities<Grid, true>;

  protected:
    BackupRestoreFacilities ()
    {}

  private:
    BackupRestoreFacilities (const Self&) = delete;
    Self& operator= (const Self&) = delete;

  protected:
    const Grid& asImp () const
    {
      return static_cast<const Grid&>(*this);
    }

    Grid& asImp ()
    {
      return static_cast<Grid&>(*this);
    }
  };

} // end namespace Curved


// BackupRestoreFacility for CurvedGrid
// -------------------------------------------

template <class HostGrid, class GridFunction, bool useInterpolation>
struct BackupRestoreFacility<CurvedGrid<HostGrid,GridFunction, useInterpolation>>
{
  using Grid = CurvedGrid<HostGrid, GridFunction, useInterpolation>;
  using HostBackupRestoreFacility = BackupRestoreFacility<HostGrid>;

  static void backup (const Grid& grid, const std::string& filename)
  {
    // notice: We should also backup the coordinate function
    HostBackupRestoreFacility::backup(grid.hostGrid(), filename);
  }

  static void backup (const Grid& grid, const std::ostream& stream)
  {
    // notice: We should also backup the coordinate function
    HostBackupRestoreFacility::backup(grid.hostGrid(), stream);
  }

  static Grid* restore (const std::string& filename)
  {
    assert(false && "Restore not yet implemented for CurvedGrid");
    return nullptr;
  }

  static Grid* restore (const std::istream& stream)
  {
    assert(false && "Restore not yet implemented for CurvedGrid");
    return nullptr;
  }
};

} // end namespace Dune

#endif // DUNE_CURVEDGRID_BACKUPRESTORE_HH
