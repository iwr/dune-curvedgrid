#ifndef DUNE_CURVEDGRID_GRIDFUNCTION_HH
#define DUNE_CURVEDGRID_GRIDFUNCTION_HH

#include <type_traits>

#include <dune/common/referencehelper.hh>
#include <dune/curvedgrid/concepts.hh>
#include <dune/curvedgrid/gridfunctions/analyticgridfunction.hh>

namespace Dune {

template<class T>
using ResolveReference_t = std::remove_reference_t<decltype(Dune::resolveRef(std::declval<T&>()))>;

/// \brief Conditionally define the GridFunction type.
/**
 * If the type `GF` is a GridFunction on the grid `Grid`, use this type as GridFunction type,
 * Otherwise it is a functor and construct an \ref AnalyticGridFunction on the `Grid`.
 **/
template <class Grid, class GF,
          class GF0 = std::decay_t<GF>,
          class GF1 = ResolveReference_t<GF0>>
using GridFunctionOf_t
  = std::conditional_t<Concept::isGridFunction<GF1,Grid>(), GF0, AnalyticGridFunction<Grid,GF0>>;

} // end namespace Dune

#endif // DUNE_CURVEDGRID_GRIDFUNCTION_HH
