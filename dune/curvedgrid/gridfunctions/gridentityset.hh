#ifndef DUNE_CURVEDGRID_GRID_ENTITYSET_HH
#define DUNE_CURVEDGRID_GRID_ENTITYSET_HH

namespace Dune {

/// \brief A set of entities of given `codim` of a `Grid`
/**
 * \tparam GridType  The grid type
 * \tparam codim     Codimension of the entities to define the set of.
 *
 * \note This entityset just defines types
 **/
template <class GridType, int codim>
class GridEntitySet
{
public:
  /// \brief Type of the grid
  using Grid = GridType;

  /// \brief Type of Elements contained in this EntitySet
  using Element = typename Grid::template Codim<codim>::Entity;

  /// \brief Type of local coordinates with respect to the Element
  using LocalCoordinate = typename Element::Geometry::LocalCoordinate;

  /// \brief Type of global coordinates with respect to the Element
  using GlobalCoordinate = typename Element::Geometry::GlobalCoordinate;
};

} // end namespace Dune

#endif // DUNE_CURVEDGRID_GRID_ENTITYSET_HH
