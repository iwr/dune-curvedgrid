#ifndef DUNE_CURVEDGRID_DISCRETE_GRIDVIEWFUNCTION_HH
#define DUNE_CURVEDGRID_DISCRETE_GRIDVIEWFUNCTION_HH

#include <array>
#include <optional>
#include <vector>

#if !HAVE_DUNE_FUNCTIONS
#error "Need dune-functions for the definition of the DiscreteGridViewFunction"
#endif

#include <dune/common/fvector.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/common/defaultderivativetraits.hh>
#include <dune/functions/functionspacebases/basistags.hh>
#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/gridfunctions/gridviewentityset.hh>
#include <dune/functions/gridfunctions/localderivativetraits.hh>
#include <dune/grid/utility/hierarchicsearch.hh>
#include <dune/istl/bvector.hh>


namespace Dune {
namespace Impl {

template <class Sig, int degree, template <class> class Traits>
struct DerivativeRangeType;

template <class R, class D, int degree, template <class> class DerivativeTraits>
struct DerivativeRangeType<R(D), degree, DerivativeTraits>
{
  using DerivativeRange = typename DerivativeTraits<R(D)>::Range;
  using type = typename DerivativeRangeType<DerivativeRange(D), degree-1, DerivativeTraits>::type;
};

template <class R, class D, template <class> class Traits>
struct DerivativeRangeType<R(D), 0, Traits>
{
  using type = R;
};

} // end namespace Impl


/// \brief Grid-view function representing a coordinate map from a reference grid to world
/**
 * \tparam GridView   The grid-view type this coordinate function is defined on
 * \tparam components Dimension of the world this coordfunction mapps into
 * \tparam ORDER      Polynomial order of the local parametrization
 * \tparam T          Type of the basis range-type and coefficient type
 **/
template <class GridView, int components = GridView::dimensionworld, int ORDER = -1, class T = double>
class DiscreteGridViewFunction;

/// \brief Generator for \ref DiscreteGridViewFunction
/**
 * \param gridView   Function that can be evaluated at global coordinates of the \ref Grid
 * \param order      Polynomial order of the local parametrization [ORDER]
 *
 * \tparam components  Number of components in the Range type
 * \tparam ORDER  Polynomial order of the local parametrization [-1]
 * \tparam T      Range-type of the basis and coefficient value-type [double]
 **/
template <int components, int ORDER = -1, class T = double, class GridView>
auto discreteGridViewFunction (const GridView& gridView, int order = ORDER)
{
  return DiscreteGridViewFunction<GridView,components,ORDER,T>{gridView, order};
}


template <class GridView, int components, int ORDER, class T>
class DiscreteGridViewFunction
{
public:
  static auto makeBasis (const GridView& gridView, int order)
  {
    namespace BF = Functions::BasisFactory;
    return BF::makeBasis(gridView, BF::power<components>(BF::lagrange<T>(order), BF::blockedInterleaved()));
  }

  using Basis = decltype(makeBasis(std::declval<GridView>(), ORDER));

public:
  using EntitySet = Functions::GridViewEntitySet<GridView,0>;

  using Domain = typename EntitySet::GlobalCoordinate;
  using Range = FieldVector<T,components>;
  using Signature = Range(Domain);

private:
  using VectorType = BlockVector<Range>;

  template <int derivativeOrder = 0>
  class LocalFunction
  {
    using LocalView = typename Basis::LocalView;
    using LocalFiniteElement = typename LocalView::Tree::ChildType::FiniteElement;
    using LocalBasis = typename LocalFiniteElement::Traits::LocalBasisType;

    template <class Sig>
    using DerivativeTraits = typename Functions::LocalDerivativeTraits<EntitySet, Functions::DefaultDerivativeTraits>::template Traits<Sig>;

  public:
    using LocalContext = typename LocalView::Element;
    using Geometry = typename LocalContext::Geometry;

    using RangeType = typename DiscreteGridViewFunction::Range;

    using Domain = typename EntitySet::LocalCoordinate;

    template <int degree>
    using DerivativeRange = typename Impl::DerivativeRangeType<RangeType(Domain), degree, DerivativeTraits>::type;

    using Range = DerivativeRange<derivativeOrder>;
    using Signature = Range(Domain);

  public:
    template <class LV>
    LocalFunction (LV&& localView, std::shared_ptr<VectorType> coords)
      : localView_(std::forward<LV>(localView))
      , coords_(std::move(coords))
    {}

    /// \brief Collect the coords from all element DOFs into a local
    /// \brief vector that can be accessed in the operator() for interpolation
    void bind (const LocalContext& element)
    {
      localView_.bind(element);

      const auto& leafNode = localView_.tree().child(0);
      localCoords_.resize(leafNode.size());

      // collect local coordinate vectors
      for (std::size_t i = 0; i < localCoords_.size(); ++i) {
        auto idx = localView_.index(leafNode.localIndex(i));
        localCoords_[i] = (*coords_)[idx[0]];
      }

      if constexpr (derivativeOrder == 1)
        geometry_.emplace(element.geometry());

      bound_ = true;
    }

    void unbind ()
    {
      localView_.unbind();
      bound_ = false;
    }

    const LocalContext& localContext () const
    {
      assert(bound_);
      return localView_.element();
    }

    /// \brief Evaluate coordinates in local coordinates
    /// \brief by interpolation of stored coords in \ref localCoords_.
    Range operator() (const Domain& local) const
    {
      static_assert(derivativeOrder < 2, "Higher-order derivatives not implemented");

      if constexpr (derivativeOrder == 0)
        return evaluateFunction(local);
      else if constexpr (derivativeOrder == 1)
        return evaluateJacobian(local);

      return Range(0);
    }

    friend LocalFunction<derivativeOrder+1> derivative (const LocalFunction& lf)
    {
      return LocalFunction<derivativeOrder+1>{lf.localView_, lf.coords_};
    }

  private:
    DerivativeRange<0> evaluateFunction (const Domain& local) const
    {
      assert(bound_);

      const auto& leafNode = localView_.tree().child(0);
      const auto& lfe = leafNode.finiteElement();

      // evaluate basis functions in local coordinate
      lfe.localBasis().evaluateFunction(local, shapeValues_);
      assert(localCoords_.size() == shapeValues_.size());

      DerivativeRange<0> x(0);
      for (std::size_t i = 0; i < localCoords_.size(); ++i)
        x.axpy(shapeValues_[i], localCoords_[i]);

      return x;
    }

    DerivativeRange<1> evaluateJacobian (const Domain& local) const
    {
      assert(bound_);
      assert(!!geometry_);

      const auto& leafNode = localView_.tree().child(0);
      const auto& lfe = leafNode.finiteElement();

      // evaluate basis functions in local coordinate
      lfe.localBasis().evaluateJacobian(local, shapeGradients_);
      assert(localCoords_.size() == shapeGradients_.size());

      // transform gradients to global coordinates
      auto jit = geometry_->jacobianInverseTransposed(local);
      gradients_.resize(shapeGradients_.size());
      for (std::size_t i = 0; i < shapeGradients_.size(); ++i)
        jit.mv(shapeGradients_[i][0], gradients_[i]);

      DerivativeRange<1> J(0);
      for (std::size_t i = 0; i < localCoords_.size(); ++i)
        for (std::size_t j = 0; j < J.N(); ++j)
          J[j].axpy(localCoords_[i][j], gradients_[i]);

      return J;
    }

  private:
    LocalView localView_;
    std::shared_ptr<VectorType> coords_;

    std::vector<RangeType> localCoords_;
    std::optional<Geometry> geometry_;
    mutable std::vector<typename LocalBasis::Traits::RangeType> shapeValues_;
    mutable std::vector<typename LocalBasis::Traits::JacobianType> shapeGradients_;
    mutable std::vector<DerivativeRange<0>> gradients_;

    bool bound_ = false;
  };

public:
  /// \brief Constructor, creates a new basis and new coordinate vector.
  DiscreteGridViewFunction (const GridView& gridView, int order = (ORDER > 0 ? ORDER : 1))
    : entitySet_(gridView)
    , basis_(std::make_shared<Basis>(makeBasis(gridView, order)))
    , coords_(std::make_shared<VectorType>())
  {
    update(gridView);
  }

  /// \brief Constructor, adopts the basis and coordinate vector.
  DiscreteGridViewFunction (std::shared_ptr<Basis> basis,
                            std::shared_ptr<VectorType> coords)
    : entitySet_{basis->gridView()}
    , basis_{std::move(basis)}
    , coords_{std::move(coords)}
  {}

  void update (const GridView& gridView)
  {
    entitySet_ = EntitySet{gridView};
    basis_->update(gridView);

    coords_->resize(basis_->size());
  }

  /// \brief evaluate in global coordinates
  Range operator() (const Domain& x) const
  {
    using Grid = typename GridView::Grid;
    using IS = typename GridView::IndexSet;

    const auto& gv = entitySet_.gridView();
    HierarchicSearch<Grid,IS> hsearch{gv.grid(), gv.indexSet()};

    auto element = hsearch.findEntity(x);
    auto geometry = element.geometry();
    auto localFct = localFunction(*this);
    localFct.bind(element);
    return localFct(geometry.local(x));
  }

  /// \brief Create a local function of this grid-function
  friend LocalFunction<0> localFunction (const DiscreteGridViewFunction& gf)
  {
    return LocalFunction<0>{gf.basis_->localView(), gf.coords_};
  }

  /// \brief obtain the stored \ref GridViewEntitySet
  const EntitySet& entitySet () const
  {
    return entitySet_;
  }

  const Basis& basis () const
  {
    return *basis_;
  }

  const VectorType& coefficients () const
  {
    return *coords_;
  }

  VectorType& coefficients ()
  {
    return *coords_;
  }

private:
  EntitySet entitySet_;
  std::shared_ptr<Basis> basis_;
  std::shared_ptr<VectorType> coords_;
};

} // end namespace Dune

#endif // DUNE_CURVEDGRID_DISCRETE_GRIDVIEWFUNCTION_HH
