#ifndef DUNE_CURVEDGRID_NORMAL_GRIDVIEWFUNCTION_HH
#define DUNE_CURVEDGRID_NORMAL_GRIDVIEWFUNCTION_HH

#include <array>
#include <vector>

#if !HAVE_DUNE_FUNCTIONS
#error "Need dune-functions for the definition of the NormalGridViewFunction"
#endif

#include <dune/common/fvector.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/basistags.hh>
#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/gridfunctions/gridviewentityset.hh>
#include <dune/grid/utility/hierarchicsearch.hh>
#include <dune/istl/bvector.hh>

namespace Dune {

/// \brief Grid-view function representing averaged normal vector
/**
 * \tparam GridView   The grid-view this grid-view-function is defined on
 * \tparam ORDER      Polynomial order of the lagrange bases used for representing the normals
 * \tparam T          Value type used for the basis and the coefficients
 **/
template <class GridView, int ORDER = -1, class T = double>
class NormalGridViewFunction
{
public:
  static auto makeBasis (const GridView& gridView, int order)
  {
    namespace BF = BasisFactory;
    return BF::makeBasis(gridView, BF::power<GridView::dimensionworld>(BF::lagrange<T>(order), BF::blockedInterleaved()));
  }

  using Basis = decltype(makeBasis(std::declval<GridView>(), ORDER));

public:
  using EntitySet = GridViewEntitySet<GridView,0>;

  using Domain = typename EntitySet::GlobalCoordinate;
  using Range = FieldVector<T,GridView::dimensionworld>;

  using VectorType = BlockVector<Range>;

private:
  class LocalFunction
  {
    using LocalView = typename Basis::LocalView;
    using LocalContext = typename LocalView::Element;

    using Domain = typename EntitySet::LocalCoordinate;
    using Range = typename NormalGridViewFunction::Range;

  public:
    LocalFunction (LocalView&& localView, std::shared_ptr<VectorType> normals)
      : localView_(std::move(localView))
      , normals_(std::move(normals))
    {}

    /// \brief Collect the normal vector from all element DOFs into a local
    /// \brief vector that can be accessed in the operator() for interpolation
    void bind (const LocalContext& localContext)
    {
      localView_.bind(localContext);

      const auto& leafNode = localView_.tree().child(0);
      localNormals_.resize(leafNode.size());

      // collect local normal vectors
      for (std::size_t i = 0; i < localNormals_.size(); ++i) {
        auto idx = localView_.index(leafNode.localIndex(i));
        localNormals_[i] = (*normals_)[idx[0]];
      }

      bound_ = true;
    }

    void unbind ()
    {
      localView_.unbind();
      bound_ = false;
    }

    // evaluate normal vectors in local coordinate
    // by interpolation of stored local normals.
    Range operator() (const Domain& local) const
    {
      assert(bound_);

      const auto& leafNode = localView_.tree().child(0);
      const auto& lfe = leafNode.finiteElement();

      // evaluate basis functions in local coordinate
      lfe.localBasis().evaluateFunction(local, shapeValues_);
      assert(localNormals_.size() == shapeValues_.size());

      Range n(0);
      for (std::size_t i = 0; i < localNormals_.size(); ++i)
        n.axpy(shapeValues_[i], localNormals_[i]);

      // return normalized vector
      return n / n.two_norm();
    }

  private:
    LocalView localView_;
    std::shared_ptr<VectorType> normals_;

    std::vector<Range> localNormals_;
    mutable std::vector<FieldVector<T,1>> shapeValues_;
    bool bound_ = false;
  };

public:
  /// \brief Constructor of the grid function.
  /**
   * Creates a global basis of a power of langrange nodes of given order.
   * The constructor argument `order` is defaulted to the class template parameter.
   **/
  NormalGridViewFunction (const GridView& gridView, int order = ORDER)
    : entitySet_(gridView)
    , basis_(std::make_shared<Basis>(makeBasis(gridView, order)))
    , normals_(std::make_shared<VectorType>())
  {
    update(gridView);
  }

  /// \brief Constructor, adopts the basis and normal vector.
  NormalGridViewFunction (std::shared_ptr<Basis> basis,
                          std::shared_ptr<VectorType> normals)
    : entitySet_{basis->gridView()}
    , basis_{std::move(basis)}
    , normals_{std::move(normals)}
  {
    update(basis_->gridView());
  }

  /// \brief Update the grid function.
  /**
   * This calculates a mean average of normal vectors in the  DOFs of the basis.
   * Those averages are stored normalized in the coefficients vector.
   **/
  void update (const GridView& gridView)
  {
    entitySet_ = EntitySet{gridView};
    basis_.update(gridView);

    auto& normals = *normals_;
    normals.resize(basis_.size());
    normals = 0;

    // compute normal vectors by mean averaging
    auto localView = basis_.localView();
    for (const auto& e : elements(basis_.gridView()))
    {
      localView.bind(e);
      auto geometry = e.geometry();

      const auto& leafNode = localView.tree().child(0);
      const auto& lfe = leafNode.finiteElement();

      // interpolate normal of geometry
      std::vector<Range> localNormals;
      lfe.localInterpolation().interpolate([&](const auto& local) -> Range {
        return Dune::normal(geometry, local);
      }, localNormals);

      // copy to global vector
      for (std::size_t i = 0; i < localNormals.size(); ++i) {
        auto idx = localView.index(leafNode.localIndex(i));
        normals[idx[0]] += localNormals[i];
      }
    }

    // normalize vector
    for (std::size_t i = 0; i < normals.size(); ++i)
      normals[i] /= normals[i].two_norm();
  }

  /// \brief Evaluate normal vectors in global coordinates
  // NOTE: expensive
  Range operator() (const Domain& x) const
  {
    using Grid = typename GridView::Grid;
    using IS = typename GridView::IndexSet;

    const auto& gv = basis_.gridView();
    HierarchicSearch<Grid,IS> hsearch{gv.grid(), gv.indexSet()};

    auto element = hsearch.findEntity(x);
    auto geometry = element.geometry();
    auto localFct = localFunction(*this);
    localFct.bind(element);
    return localFct(geometry.local(x));
  }

  /// \brief Create a local function of this grid-function
  friend LocalFunction localFunction (const NormalGridViewFunction& gf)
  {
    return LocalFunction{gf.basis_->localView(), gf.normals_};
  }

  /// \brief obtain the stored \ref GridViewEntitySet
  const EntitySet& entitySet () const
  {
    return entitySet_;
  }

private:
  EntitySet entitySet_;
  std::shared_ptr<Basis> basis_;
  std::shared_ptr<VectorType> normals_;
};

} // end namespace Dune

#endif // DUNE_CURVEDGRID_NORMAL_GRIDVIEWFUNCTION_HH
