#ifndef DUNE_CURVEDGRID_ANALYTIC_GRIDFUNCTION_HH
#define DUNE_CURVEDGRID_ANALYTIC_GRIDFUNCTION_HH

#include <optional>
#include <type_traits>
#include <utility>

#include <dune/common/typeutilities.hh>

#include "gridentityset.hh"

namespace Dune {


/// \brief LocalFunction associated to the \ref LocalAnalyticGridFunction
/**
 * \param LC  The local context, e.g., element or intersection.
 * \param F   Type of the function that can be evaluated in global coordinates.
 */
template <class LC, class F>
class LocalAnalyticGridFunction
{
public:
  using LocalContext = LC;
  using Geometry = typename LocalContext::Geometry;
  using Functor = F;

  using Domain = typename Geometry::GlobalCoordinate;
  using LocalDomain = typename Geometry::LocalCoordinate;
  using Range = std::invoke_result_t<Functor,Domain>;
  using Signature = Range(LocalDomain);

public:
  /// \brief Constructor. Stores the functor f by value
  template <class FF,
    disableCopyMove<LocalAnalyticGridFunction, FF> = 0>
  explicit LocalAnalyticGridFunction (FF&& f)
    : f_(std::forward<FF>(f))
  {}

  /// \brief Constructor that copies the localContext and the geometry
  LocalAnalyticGridFunction (const Functor& f,
                             const std::optional<LocalContext>& localContext,
                             const std::optional<Geometry>& geometry)
    : f_(f)
    , localContext_(localContext)
    , geometry_(geometry)
  {}

  /// \brief bind the LocalFunction to the local context
  /**
    * Stores the localContext and its geometry in a cache variable
    **/
  void bind (const LocalContext& localContext)
  {
    localContext_.emplace(localContext);
    geometry_.emplace(localContext_->geometry());
  }

  /// \brief unbind the localContext from the localFunction
  /**
    * Reset the geometry
    **/
  void unbind ()
  {
    geometry_.reset();
    localContext_.reset();
  }

  /// \brief evaluate the stored function in local coordinates
  /**
    * Transform the local coordinates to global coordinates first,
    * then evaluate the stored functor.
    **/
  Range operator() (const LocalDomain& x) const
  {
    assert(!!geometry_);
    return f_(geometry_->global(x));
  }

  /// \brief return the bound localContext.
  const LocalContext& localContext () const
  {
    assert(!!localContext_);
    return *localContext_;
  }

  /// \brief return the bound geometry.
  const Geometry& geometry () const
  {
    assert(!!geometry_);
    return *geometry_;
  }

  /// \brief obtain the functor
  const Functor& f () const
  {
    return f_;
  }

private:
  Functor f_;

  // some caches
  std::optional<LocalContext> localContext_;
  std::optional<Geometry> geometry_;
};

/// \brief Derivative of an \ref LocalAnalyticGridFunction
/**
 * Participates in overload resolution only if the functor `F` is differentiable
 **/
template <class LC, class F,
  class DF = std::decay_t<decltype(derivative(std::declval<const F&>()))>>
auto derivative (const LocalAnalyticGridFunction<LC,F>& lf)
  -> LocalAnalyticGridFunction<LC,DF>
{
  return {derivative(lf.f()), lf.localContext(), lf.geometry()};
}



/// \brief GridFunction associated to the mapping F
/**
 * \tparam G   The grid type with elements the corresponding LocalFunction can be bound to
 * \tparam F   Type of a function that can be evaluated in global coordinates.
 **/
template <class G, class F>
class AnalyticGridFunction
{
public:
  using Grid = G;
  using EntitySet = GridEntitySet<Grid,0>;
  using Functor = F;

  using Domain = typename EntitySet::GlobalCoordinate;
  using Range = std::invoke_result_t<Functor,Domain>;
  using Signature = Range(Domain);

  using LocalFunction = LocalAnalyticGridFunction<typename EntitySet::Element,Functor>;

public:
  /// \brief Constructor. Stores the functor f by value
  template <class FF,
    disableCopyMove<AnalyticGridFunction, FF> = 0>
  explicit AnalyticGridFunction (FF&& f)
    : f_(std::forward<FF>(f))
  {}

  /// \brief evaluate the stored function in global coordinates
  Range operator() (const Domain& x) const
  {
    return f_(x);
  }

  /// \brief construct the \ref LocalAnalyticGridFunction
  friend LocalFunction localFunction (const AnalyticGridFunction& t)
  {
    return LocalFunction{t.f_};
  }

  /// \brief obtain the stored \ref GridEntitySet
  const EntitySet& entitySet () const
  {
    return entitySet_;
  }

  /// \brief obtain the functor
  const Functor& f () const
  {
    return f_;
  }

private:
  Functor f_;
  EntitySet entitySet_;
};


/// \brief Derivative of an \ref AnalyticGridFunction
/**
 * Participates in overload resolution only if the functor `F` is differentiable
 **/
template <class G, class F,
  class DF = std::decay_t<decltype(derivative(std::declval<const F&>()))>>
auto derivative (const AnalyticGridFunction<G,F>& gf)
  -> AnalyticGridFunction<G,DF>
{
  return {derivative(gf.f())};
}

/// \brief Generator for \ref AnalyticGridFunction
/**
 * \param  ff    Function that can be evaluated at global coordinates of the \ref Grid
 * \tparam Grid  The grid type with elements the corresponding LocalFunction can be bound to
 **/
template <class Grid, class FF>
auto analyticGridFunction (FF&& ff)
{
  using F = std::decay_t<FF>;
  return AnalyticGridFunction<Grid, F>{std::forward<FF>(ff)};
}

template <class FF, class Grid>
auto analyticGridFunction (FF&& ff, Grid const& /*grid*/)
{
  using F = std::decay_t<FF>;
  return AnalyticGridFunction<Grid, F>{std::forward<FF>(ff)};
}

} // end namespace Dune

#endif // DUNE_CURVEDGRID_ANALYTIC_GRIDFUNCTION_HH
