#ifndef DUNE_CURVEDGRID_CONCEPTS_HH
#define DUNE_CURVEDGRID_CONCEPTS_HH

#include <dune/common/concept.hh>

namespace Dune {
namespace Concept {

/// \brief Concept: objects that can be called with given argument list `Args...`
template <class... Args>
struct Callable
{
  template <class F >
  auto require (F&& f) -> decltype(
    f(std::declval<Args>()...)
  );
};

/// \brief Check if `F` models the `Callable` concept with the given arguments `Args...`
template <class F, class... Args>
constexpr auto isCallable ()
{ return models<Concept::Callable<Args...>, F>(); }


/// \brief Concept: function associated to the `LocalContext` that can be evaluated in local coordinates
template <class LocalContext>
struct LocalFunction
{
  using LocalCoordinate = typename LocalContext::Geometry::LocalCoordinate;

  template <class LF>
  auto require (LF&& lf) -> decltype(
    lf.bind(std::declval<LocalContext const&>()),
    lf.unbind(),
    lf.localContext(),
    requireConcept<Concept::Callable<LocalCoordinate>>(lf),
    requireConvertible<LocalContext>(lf.localContext())
  );
};

/// \brief Check if `LF` models the `LocalFunction` concept on the given `LocalContext`
template <class LF, class LocalContext>
constexpr bool isLocalFunction ()
{ return models<Concept::LocalFunction<LocalContext>, LF>(); }


/// \brief Concept: function associated to the `Grid` that can be evaluated in global coordinates
template <class Grid>
struct GridFunction
{
  using LocalContext = typename Grid::template Codim<0>::Entity;
  using GlobalCoordinate = typename LocalContext::Geometry::GlobalCoordinate;

  template <class GF >
  auto require (GF&& gf) -> decltype(
    localFunction(gf),
    gf.entitySet(),
    requireConcept<Concept::Callable<GlobalCoordinate>>(gf),
    requireConcept<Concept::LocalFunction<LocalContext>>(localFunction(gf))
  );
};

/// \brief Check if `GF` models the `GridFunction` concept on the given `Grid`
template <class GF, class Grid>
constexpr bool isGridFunction ()
{ return models<Concept::GridFunction<Grid>, GF>(); }

} // end namespace Concept
} // end namespace Dune

#endif // DUNE_CURVEDGRID_CONCEPTS_HH