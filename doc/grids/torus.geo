h = 0.75;
r1 = 2.0;
r2 = 1.0;

Point(1) = {r1,    0.0, 0.0, h};
Point(2) = {r1+r2, 0.0, 0.0, h};
Point(3) = {r1,    0.0, r2,  h};
Point(4) = {r1-r2, 0.0, 0.0, h};
Point(5) = {r1,    0.0,-r2,  h};

Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
Line Loop(5) = {1,2,3,4};

first[]  = Extrude {{0,0,2}, {0,0,0}, -2*Pi/3}{Line{1,2,3,4};                                  Layers{10};};
second[] = Extrude {{0,0,2}, {0,0,0}, -2*Pi/3}{Line{first[0],first[4],first[8],first[12]};     Layers{10};};
third[]  = Extrude {{0,0,2}, {0,0,0}, -2*Pi/3}{Line{second[0],second[4],second[8],second[12]}; Layers{10};};
