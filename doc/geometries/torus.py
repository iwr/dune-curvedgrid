from sympy import *

x, y, z = symbols("x y z", real=True)
R, r = symbols("R r", positive=True)

X = [x,y,z]

# implicit representation of the surface
phi0 = sqrt(x**2 + y**2) - R
phi = phi0**2 + z**2 - r**2
print("phi = ")
print("return ",ccode(simplify(phi)),";")

# gradient of phi
Jphi = [diff(phi,X[i]) for i in range(3)]
print("Jphi = ")
print("return {")
for i in range(3):
  print("    ",ccode(simplify(Jphi[i])),",")
print("};")
print("")
