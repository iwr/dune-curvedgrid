from sympy import *

x, y, z = symbols("x y z", real=True)
a, b, c = symbols("a b c", positive=True)

X = [x,y,z]

# implicit representation of the surface
phi = x**2/a**2 + y**2/b**2 + z**2/c**2 - 1
print("phi = ")
print("return ",ccode(simplify(phi)),";")

# gradient of phi
Jphi = [diff(phi,X[i]) for i in range(3)]
print("Jphi = ")
print("return {")
for i in range(3):
  print("    ",ccode(simplify(Jphi[i])),",")
print("};")
print("")
